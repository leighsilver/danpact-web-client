// @flow
import * as React from 'react';
import { withFormik } from 'formik';
import { startCase, omit, flowRight } from 'lodash';
import sentence from 'sentence-case';
import Yup from 'yup';
import reset from '../reset';

type $props = Object;

export default (properties: Object) => {
  const last = {};
  const mapPropsToValues = props => {
    const initialValue = properties.mapPropsToValues(props);
    if (!last.initialValue) {
      last.initialValue = initialValue;
    }
    return last.initialValue;
  };
  const getFields = (
    values,
    errors,
    touched,
    setFieldValue,
    setFieldTouched,
    setFieldError,
    handleChange
  ) => {
    if (
      last.values !== values ||
      last.errors !== errors ||
      last.touched !== touched
    ) {
      last.values = values;
      last.errors = errors;
      last.touched = touched;
      const {array}  = properties;
      const arrayResult = {}
      const blacklistKeys = [];
      const onChangeFactory = (key) => value => setFieldValue(key, value, true);
      if(array){
        Object.keys(array).forEach((arrayProp)=>{
          const rawSchemaValidation = array[arrayProp];
          arrayResult[arrayProp] = [];
          let i = 0;
          while(i >= 0){
            Object.keys(rawSchemaValidation).forEach((currentKey)=>{
              if (i === -1){
                return;
              }
              const key = `${arrayProp}_${i}_${currentKey}`
              const onChange = onChangeFactory(key);
              if(values.hasOwnProperty(key)){

                if (!arrayResult[arrayProp][i]) {
                  arrayResult[arrayProp][i] = { key };
                }
                blacklistKeys.push(key);
                const value = values[key];
                const j = i;              
                const getNextArray = (currentArray, value)=>{
                  const currentItem = currentArray[j];
                  currentItem[currentKey] = value;
                  return currentArray;
                }
                
                
                arrayResult[arrayProp][i][currentKey] = {
                  name: key,
                  label: startCase(sentence(currentKey)),
                  value,
                  error: touched[key] ? errors[key] : '',
                  onChange: handleChange ? handleChange(key, onChange, arrayProp, getNextArray) : onChange,
                  onError: (value, error) => {
                    onChange(value);
                    setFieldError(key, error);
                  },
                  onBlur: () => setFieldTouched(key, true)
                };
              } else {
                i = -1;
              }
            })
            if (i >= 0)
              i++
          }
        })
      }

      last.result = Object.keys(omit(values, blacklistKeys)).reduce((finalResult, key) => {
        const onChange = onChangeFactory(key);
        finalResult[key] = {
          name: key,
          label: startCase(sentence(key)),
          value: values[key],
          error: touched[key] ? errors[key] : '',
          onChange: handleChange ? handleChange(key, onChange) : onChange,
          onError: (value, error) => {
            onChange(value);
            setFieldError(key, error);
          },
          onBlur: () => setFieldTouched(key, true)
        };
        return finalResult;
      }, {arrayFields: arrayResult});
    }
    return last.result;
  };
  return (Comp: React.ComponentType<any>) => {
    class Form extends React.Component<$props> {
      componentWillMount() {
        if (this.props.passUpReinitializeForm)
          this.props.passUpReinitializeForm(this.reinitializeForm, properties.propName);
      }
      reinitializeForm = () => {
        last.initialValue = undefined;
        this.props.reset();
      };
      render() {
        const {
          values,
          errors,
          touched,
          setFieldValue,
          setFieldTouched,
          setFieldError,
          isValid,
          isSubmitting,

          // dirty,
          // get,
          // handleBlur,
          // handleChange,
          // handleReset,
          // handleSubmit,
          // id,
          // initialValues,
          // passUpReinitializeForm,
          // pitchForm,
          // reinitializeForm,
          // reset,
          // resetForm,
          // setError,
          // setErrors,
          // setFormikState,
          // setStatus,
          // setSubmitting,
          // setTouched,
          // setValues,
          // submitForm,
          // talent,
          // updateTalent,
          // user,
          // validateForm,
          // validateOnBlur,
          // validateOnChange,
          ...props
        } = this.props;
        const fields = getFields(
          values,
          errors,
          touched,
          setFieldValue,
          setFieldTouched,
          setFieldError,
          properties.handleChange
            ? properties.handleChange(props)
            : undefined
        );

        const otherProps = {
          isValid,
          isSubmitting,
        }
        let fullProps = { ...props };
        if (properties.propName){
          fullProps[properties.propName] = {
            fields,
            ...otherProps,
          }
        } else {
          fullProps = {...props, ...otherProps, fields};
        }
        return (
          <Comp
            {...fullProps}
          />
        );
      }
    }
    return flowRight([
      reset,
      withFormik({ ...properties, mapPropsToValues, enableReinitialize: true })
    ])(Form);
  };
};

export const getArrayValidationSchema = (mapPropsToValues: Function, arrayPropsObject: Object, validationSchema : Object = {})=>(props: Object)=>{
  const initialValues = mapPropsToValues(props);
  return Yup.object().shape(
    Object.keys(arrayPropsObject).reduce((finalResult, arrayProp)=>{
      const rawValidationSchema = arrayPropsObject[arrayProp]
      return initialValues[arrayProp].reduce((accum, current, index)=>({
        ...accum,
        ...Object.keys(rawValidationSchema).reduce((accum2, key)=>{
          accum2[`${arrayProp}_${index}_${key}`] = rawValidationSchema[key];
          return accum2;
        }, {}),
      }), finalResult);
    }, validationSchema)
  )
}

export const getMapPropsToValues = (baseMapPropsToValues: Function, arrayPropsObject: Object)=>(props: Object) => {
  const initialValues = baseMapPropsToValues(props);
  return Object.keys(arrayPropsObject).reduce((finalResult, arrayProp)=>{
    return {
      ...finalResult,
      ...initialValues[arrayProp].reduce((accum, current, index) => ({
        ...accum,
        ...Object.keys(arrayPropsObject[arrayProp]).reduce((accum2, key)=>{
          accum2[`${arrayProp}_${index}_${key}`] = current[key]  
          return accum2
        }, {})
      }), {})
    }
  }, initialValues);
}