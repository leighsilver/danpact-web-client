// @flow
import * as React from 'react';

export default (Comp: *) =>
  class FormParent extends React.PureComponent<*> {
    reinitializeForm = {
      go: ()=>{},
    };
    passUpReinitializeForm = (reinitializeForm: Function, propName: string) => {
      const oldFunction = this.reinitializeForm.go
      this.reinitializeForm.go = ()=>{
        oldFunction();
        reinitializeForm();
      }
      this.reinitializeForm[propName] = reinitializeForm;
    };
    render() {
      return (
        <Comp
          {...this.props}
          reinitializeForm={this.reinitializeForm}
          passUpReinitializeForm={this.passUpReinitializeForm}
        />
      );
    }
  };
