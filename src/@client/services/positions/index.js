// @flow
import {baseServices} from '@client/utils/services'
import uuid from 'uuid/v1';

const base = baseServices('positions')

module.exports = {
  ...base,
  get: (id)=>Promise.resolve({id, talents: [{ id: uuid() }]}),
  my: ()=>Promise.resolve([
    {id: uuid()}
  ])
};
