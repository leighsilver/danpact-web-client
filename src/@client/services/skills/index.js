// @flow
import {baseServices} from '@client/utils/services'

const base = baseServices('skills')

module.exports = {
  ...base,
  search: (text)=>{
    return base.post('search', {text})
  },
}

