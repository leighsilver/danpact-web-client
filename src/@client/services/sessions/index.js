// @flow
import { baseServices } from '@client/utils/services';

const base = baseServices('sessions');

module.exports = {
  ...base,
  checkIfLoggedIn: base.index,
};
