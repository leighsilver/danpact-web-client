// @flow
import uuid from 'uuid/v1';
import {baseServices} from '@client/utils/services'

const base = baseServices('discourses')

module.exports = {
  ...base,
  get: (id)=>Promise.resolve({
    id,
    comments: [{
      id: uuid()
    }, {
      id: uuid()
    }],
    position: {
      id: uuid(),
    },
    company: {
      id: uuid()
    }
  })
}

