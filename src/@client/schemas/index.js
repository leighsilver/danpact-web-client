// @flow
import { validateSchema } from 'normer';
import users from './users';
import tests from './tests';
import talents from './talents';
import discourses from './discourses';
import companies from './companies';
import comments from './comments';
import positions from './positions';

const schema = {
  users,
  tests,
  talents,
  discourses,
  companies,
  comments,
  positions,
};
validateSchema(schema);

module.exports = schema;
