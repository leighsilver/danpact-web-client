// @flow
import { validateSchema } from 'normer';
import sessions from './sessions';
import homes from './homes';

const schema = {
  sessions,
  homes
};

validateSchema(schema);

module.exports = schema;
