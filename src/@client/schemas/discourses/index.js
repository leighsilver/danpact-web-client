// @flow
import { relationshipTypes } from 'normer';
import {standardize} from '@client/utils/schemas'
import Model, { properties } from '@client/models/Discourse';

const relationships = [
  {
    name: 'position',
    entityName: 'positions',
    type: relationshipTypes.ONE,
  },
  {
    name: 'company',
    entityName: 'companies',
    type: relationshipTypes.ONE,
  },
  {
    name: 'comments',
  },
]

export default standardize({properties, relationships})
