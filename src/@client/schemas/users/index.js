// @flow
import { relationshipTypes } from 'normer';
import { properties } from '@client/models/User';
import { standardize } from '@client/utils/schemas';

const relationships = [
  {
    name: 'discourses'
  },
  {
    name: 'talent',
    entityName: 'talents',
    type: relationshipTypes.ONE,
  },
];

export default standardize({ properties, relationships });
