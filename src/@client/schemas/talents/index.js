// @flow
import { relationshipTypes } from 'normer';
import {standardize} from '@client/utils/schemas'
import Model, { properties } from '@client/models/Talent';

const relationships = [{
  name: 'user',
  entityName: 'users',
  type: relationshipTypes.ONE,
}];

export default standardize({properties, relationships})

