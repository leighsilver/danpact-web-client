// @flow
import { relationshipTypes } from 'normer';
import {standardize} from '@client/utils/schemas'
import Model, { properties } from '@client/models/Position';

const relationships = [{
  name: 'company',
  entityName: 'companies',
  type: relationshipTypes.ONE,
},{
  name: 'talents'
}]

export default standardize({properties, relationships})

