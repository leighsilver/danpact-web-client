// @flow
import Model from '@client/models/Skill'
import schema from '@client/schemas/skills'
import {entityReducer, relationshipReducer, getValueFunc} from '@client/utils/reducers'

const entityName = 'skills'

export const relationship = {
  ...relationshipReducer(entityName, getValueFunc(schema)),
}

export const entity = {
  ...entityReducer(entityName, (ent)=>new Model(ent)),
}

export const initialState = {
  
}