// @flow
import Model from '@client/models/Discourse'
import schema from '@client/schemas/discourses'
import {entityReducer, relationshipReducer, getValueFunc} from '@client/utils/reducers'

const entityName = 'discourses'

export const relationship = {
  ...relationshipReducer(entityName, getValueFunc(schema)),
}

export const entity = {
  ...entityReducer(entityName, (ent)=>new Model(ent)),
}

export const initialState = {
  
}