// @flow
import Model from '@client/models/Position'
import schema from '@client/schemas/positions'
import {entityReducer, relationshipReducer, getValueFunc} from '@client/utils/reducers'

const entityName = 'positions'

export const relationship = {
  ...relationshipReducer(entityName, getValueFunc(schema)),
}

export const entity = {
  ...entityReducer(entityName, (ent)=>new Model(ent)),
}

export const initialState = {
  
}