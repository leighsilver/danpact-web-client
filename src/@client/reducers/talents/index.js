// @flow
import Model from '@client/models/Talent'
import schema from '@client/schemas/talents'
import {entityReducer, relationshipReducer, getValueFunc} from '@client/utils/reducers'

const entityName = 'talents'

export const relationship = {
  ...relationshipReducer(entityName, getValueFunc(schema)),
}

export const entity = {
  ...entityReducer(entityName, (ent)=>new Model(ent)),
}

export const initialState = {
  
}