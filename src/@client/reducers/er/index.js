// @flow
import { Map } from 'immutable';
import * as users from '../users';
import * as pages from '../pages';
import * as tests from '../tests';
import * as talents from '../talents';
import * as discourses from '../discourses';
import * as companies from '../companies';
import * as comments from '../comments';
import * as positions from '../positions';

const all = {
  users,
  pages,
  tests,
  talents,
  discourses,
  companies,
  comments,
  positions,
};

const splitReducers = allReducers => {
  return Object.keys(allReducers).reduce(
    (finalResult, reducerName) => {
      finalResult.entities[reducerName] = allReducers[reducerName].entity;
      finalResult.relationships[reducerName] =
        allReducers[reducerName].relationship;
      finalResult.initialState.entities[reducerName] = new Map(
        // $FlowFixMe
        allReducers[reducerName].initialState.entity
      );
      finalResult.initialState.relationships[reducerName] = new Map(
        // $FlowFixMe
        allReducers[reducerName].initialState.relationship
      );
      return finalResult;
    },
    {
      entities: {},
      relationships: {},
      initialState: {
        entities: {},
        relationships: {}
      }
    }
  );
};

module.exports = splitReducers(all);
