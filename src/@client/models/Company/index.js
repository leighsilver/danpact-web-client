// @flow
import { Record } from 'immutable';

export const properties = {
  id: 0,
  name: 'The first company',
};
export default class Company extends Record(properties) {

}

