// @flow
import { fromJS, List, Record, Map } from 'immutable';
import { transform } from '@client/utils/models';

export const properties = {
  id: 0,
  jobSubtypes: List(),
  workSkills: List(),
  sideProjectSkills: List(),
  work: fromJS([]),
  experience: Map(),
  education: fromJS([]),
  culture: '',
  companySize: '',
  snippet: '',
  previousWork: '',
  nextWork: '',
  compensation: 50000,
  vacation: 1,
  cities: List(),
  remote: 0,
  relocate: true,
  advancement: true,
  diversity: '',
  layout: '',
  games: '',
  social: '',
  homeWork: '',
  personalWebsite: '',
  blog: '',
  github: '',
  linkedIn: '',
  facebook: '',
  twitter: '',
  stackOverflow: '',
  resume: '',
  overtime: '',
  weeklyHours: '',
  active: false,
  blockCompanies: List()
};

export default class Talent extends Record(properties) {
  constructor(opts: Object = {}){
    super(transform(opts, {
      education: (obj = [])=>fromJS(obj),
      work: (obj = [])=>fromJS(obj),
      jobSubtypes: List,
      workSkills: List,
      sideProjectSkills: List,
      cities: List,
      experience: (obj = {})=>fromJS(obj),
      blockCompanies: (obj = [])=>fromJS(obj),

    }))
  }
}

