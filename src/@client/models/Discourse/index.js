// @flow
import { Record } from 'immutable';

export const properties = {
  id: 0,
  compensation: 50000,
  vacation: '6 weeks',
  status: 1,
};
export default class Discourse extends Record(properties) {

}

export const status = {
  INTERVIEW_OFFERED: {
    id: 1,
    label: 'Request'
  },
  INTERVIEW_ACCEPTED: {
    id: 2,
    label: 'Accepted'
  },
  INTERVIEW_COMPLETED: {
    id: 3,
    label: 'Completed',
  },
  INTERVIEW_FINAL_OFFER: {
    id: 4,
    label: 'Employment Offer'
  },
  INTERVIEW_REJECTED: {
    id: 5,
    label: 'Rejected',
  },
  INTERVIEW_FINAL_OFFER_ACCEPTED: {
    id: 6,
    label: 'Employment Offer Accepted'
  },
  INTERVIEW_FINAL_OFFER_REJECTED: {
    id: 7,
    label: 'Employment Offer Rejected',
  },
}

export const statusById = Object.values(status).reduce((finalResult, stat)=>{
  // $FlowFixMe
  finalResult[stat.id] = stat.label;
  return finalResult;
}, {})

export const displayStatus = (statusId : number)=>{
  return statusById[statusId];
}