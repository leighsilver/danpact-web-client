// @flow
import { Record } from 'immutable';

export const properties = {
  id: '',
  fullName: 'Example Name',
  city: '',
  description: '',
};

export default class User extends Record(properties) {}
