// @flow
import { Record } from 'immutable';

export const properties = {
  id: 0,
  content: 'This is sample content for a comment',
};
export default class Comment extends Record(properties) {

}

