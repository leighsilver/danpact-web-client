// @flow
import { Record } from 'immutable';

export const properties = {
  id: 'fakeId',
  title: 'This is a sample position title',
  type: 1,
  subtype: 2,
  experience: 3,
  location: 'Toronto, ON',
  remote: false,
  relocate: false,
  sponsorship: false,
  // skills: List(),
};
export default class Position extends Record(properties) {
  
}
