// @flow

export const jobTypes = {
  developer: {
    id: 1,
    name: 'Developer',
    subtypes: {
      fullstack: {
        id: 1001,
        name: 'Full-stack',
      },
      frontend: {
        id: 1002,
        name: 'Front-end',
      },
      backend: {
        id: 1003,
        name: 'Back-end',
      },
      mobile: {
        id: 1004,
        name: 'Mobile',
      },
      desktop: {
        id: 1005,
        name: 'Desktop / Enterprise Applications',
      },
      gaming: {
        id: 1006,
        name: 'Gaming'
      },
      embedded: {
        id: 1007,
        name: "Embedded"
      },
      machineLearning: {
        id: 1008,
        name: 'Machine Learning'
      },
      hardware: {
        id: 1009,
        name: 'Hardware'
      },
      nlp: {
        id: 1010,
        name: 'NLP'
      },
      arvr: {
        id: 1011,
        name: 'AR / VR'
      },
      vision: {
        id: 1012,
        name: 'Vision',
      },
      blockchain: {
        id: 1013,
        name: 'Blockchain'
      },
      search: {
        id: 1014,
        name: 'Search'
      },
      security: {
        id: 1015,
        name: 'Security'
      },
      data: {
        id: 1016,
        name: 'Data'
      },
    }
  }
}

export const jobSubtypesById = Object.values(jobTypes.developer.subtypes).reduce((finalResult, subtype)=>{
  finalResult[subtype.id] = subtype.name;
  return finalResult;
}, {})