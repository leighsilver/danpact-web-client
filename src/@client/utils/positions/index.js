// @flow
import Yup from 'yup'

export const validationSchema = Yup.object().shape({
  title: Yup.string().required(),
  type: Yup.number(),
  subtype: Yup.number(),
  experience: Yup.number(),
  location: Yup.string().required(),
  remote: Yup.bool(),
  relocate: Yup.bool(),
  sponsorship: Yup.bool(),
})