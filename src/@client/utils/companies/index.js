// @flow
import Yup from 'yup'

export const generalValidationSchema = Yup.object().shape({
  name: Yup.string().required(),
  size: Yup.number().required(),
})