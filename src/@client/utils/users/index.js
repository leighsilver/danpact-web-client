// @flow
import Yup from 'yup'

export const validationSchema = Yup.object().shape({
  fullName: Yup.string().required(),
  city: Yup.mixed().required('You must select a city'),
});
