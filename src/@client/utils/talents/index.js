// @flow
import Yup from 'yup'

export const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  general: Yup.string(),
})

export const pitchValidationSchema = Yup.object().shape({
  snippet: Yup.string().required(),
  previousWork: Yup.string().required(),
  nextWork: Yup.string().required(),
});

export const jobValidationSchema = Yup.object().shape({
  jobSubtypes: Yup.array().of(Yup.string().required()).required(),
  experience: Yup.object().required(),
  workSkills: Yup.array().of(Yup.string().required()).required(),
  sideProjectSkills: Yup.array().of(Yup.string().required()).required(),
});

export const preferencesValidationSchema = Yup.object().shape({
  cities: Yup.array().of(Yup.string().required()).required(),
  remote: Yup.number().required(),
  relocate: Yup.boolean().required(),
  culture: Yup.number().required(),
  diversity: Yup.number().required(),
  layout: Yup.number().required(),
  games: Yup.number().required(),
  social: Yup.number().required(),
  homeWork: Yup.number().required(),
});

export const requirementsValidationSchema = Yup.object().shape({
  advancement: Yup.number().required(),
  compensation: Yup.number().required(),
  vacation: Yup.number().required(),
  overtime: Yup.number().required(),
});

export const rawSchoolValidationSchema = {
  type: Yup.number().required(),
  name: Yup.string().required(),
  degree: Yup.string().required(),
  graduationYear: Yup.number().max(2030).min(1920).required(),
  additionalComments: Yup.string(),
}

export const schoolValidationSchema = Yup.object().shape(rawSchoolValidationSchema);

export const educationValidationSchema = Yup.object().shape({
  education: Yup.array().of(schoolValidationSchema.required()).required(),
});

export const rawPositionValidationSchema = {
  company: Yup.string().required(),
  position: Yup.string().required(),
  startMonth: Yup.number().required(),
  startYear: Yup.number().required(),
  endMonth: Yup.number().required().when('current', {
    is: true,
    otherwise: Yup.number(),
  }),
  endYear: Yup.number().required().when('current', {
    is: true,
    otherwise: Yup.number(),
  }),
  current: Yup.boolean(),
  description: Yup.string(),
}

export const positionValidationSchema = Yup.object().shape(rawPositionValidationSchema);

export const workValidationSchema = Yup.object().shape({
  work: Yup.array().of(positionValidationSchema.required()).required(),
});

export const linksValidationSchema = Yup.object().shape({
  personalWebsite: Yup.string(),
  blog: Yup.string(),
  github: Yup.string(),
  linkedIn: Yup.string(),
  facebook: Yup.string(),
  twitter: Yup.string(),
  stackOverflow: Yup.string(),
  resume: Yup.string(),
})
