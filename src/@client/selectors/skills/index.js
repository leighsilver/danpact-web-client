// @flow
import { getBaseSelectors } from '@client/utils/selectors';
import Model from '@client/models/Skill'

const base = getBaseSelectors('skills', new Model())

module.exports = {
  ...base,
}