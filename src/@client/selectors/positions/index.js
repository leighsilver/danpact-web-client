// @flow
import { getBaseSelectors } from '@client/utils/selectors';
import Model from '@client/models/Position'

const base = getBaseSelectors('positions', new Model())

module.exports = {
  ...base,
}