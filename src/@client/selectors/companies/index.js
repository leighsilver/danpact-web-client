// @flow
import { getBaseSelectors } from '@client/utils/selectors';
import Model from '@client/models/Company'

const base = getBaseSelectors('companies', new Model())

module.exports = {
  ...base,
}