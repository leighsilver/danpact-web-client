// @flow
import { getBaseSelectors } from '@client/utils/selectors';
import Model from '@client/models/Talent'

const base = getBaseSelectors('talents', new Model())

module.exports = {
  ...base,
}