// @flow
import { getBaseSelectors } from '@client/utils/selectors';
import Model from '@client/models/Discourse'

const base = getBaseSelectors('discourses', new Model())

module.exports = {
  ...base,
}