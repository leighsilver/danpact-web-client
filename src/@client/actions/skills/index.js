// @flow

import { baseActions } from '@client/utils/actions'
import { entity } from '@client/reducers/skills'
import service from '@client/services/skills'
import { push } from '@client/actions/router'

const base = baseActions('skills', entity, service)

module.exports = {
  ...base,
  search: (text: string, ids: string[])=>(dispatch)=>{
    return Promise.all([
      service.search(text),
      service.getBatch(ids),
    ]).then(([searchResults, batchResults])=>[...searchResults, ...batchResults])
  },
  goToSkill: (id)=>push(`/skills/${id}`),
  goToEditSkill: (id)=>push(`/skills/${id}/edit`)
}