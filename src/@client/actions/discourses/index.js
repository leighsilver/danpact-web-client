// @flow

import { baseActions } from '@client/utils/actions'
import { entity } from '@client/reducers/discourses'
import service from '@client/services/discourses'
import { push } from '@client/actions/router'

const base = baseActions('discourses', entity, service)

module.exports = {
  ...base,
  goToDiscourse: (id)=>push(`/discourses/${id}`),
  goToEditDiscourse: (id)=>push(`/discourses/${id}/edit`)
}