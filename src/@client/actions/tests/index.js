// @flow

import { baseActions } from '@client/utils/actions';
import { entity } from '@client/reducers/tests';
import service from '@client/services/tests';
import { push } from '@client/actions/router';

const base = baseActions('tests', entity, service);

module.exports = {
  ...base,
  goToTest: id => push(`/tests/${id}`),
  goToEditTest: id => push(`/tests/${id}/edit`),
};
