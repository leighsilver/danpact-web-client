// @flow

import { baseActions, erActions } from '@client/utils/actions'
import { entity } from '@client/reducers/talents'
import service from '@client/services/talents'
import { push } from '@client/actions/router'

const base = baseActions('talents', entity, service)
const erBase = erActions('talents');

module.exports = {
  ...base,
  updateLocal: (id, props)=>erBase.get({id, ...props}),
  goToTalent: (positionId, id)=>push(`/positions/${positionId}/talents/${id}`),
}