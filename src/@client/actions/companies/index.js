// @flow

import { baseActions } from '@client/utils/actions'
import { entity } from '@client/reducers/companies'
import service from '@client/services/companies'
import { push } from '@client/actions/router'

const base = baseActions('companies', entity, service)

module.exports = {
  ...base,
  goToCompany: (id)=>push(`/companies/${id}`),
  goToEditCompany: (id)=>push(`/companies/${id}/edit`)
}