// @flow
import positionService from '@client/services/positions';
import { pageERActions } from '@client/utils/actions';
import { push } from '@client/actions/router';
const base = pageERActions('homes');

module.exports = {
  ...base,
  goHome: ()=>{
    return push('/');
  },
  getPositions: ()=>(dispatch)=>{
    return positionService.my().then((positions)=>{
      dispatch(
        base.get({
          positions,
        })
      )
    })
  }
};
