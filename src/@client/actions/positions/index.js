// @flow

import { baseActions } from '@client/utils/actions'
import { entity } from '@client/reducers/positions'
import service from '@client/services/positions'
import { push } from '@client/actions/router'

const base = baseActions('positions', entity, service)

module.exports = {
  ...base,
  goToPosition: (id)=>push(`/positions/${id}`),
  goToEditPosition: (id)=>push(`/positions/${id}/edit`)
}