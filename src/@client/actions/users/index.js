// @flow
import { baseActions, erActions } from '@client/utils/actions';
import { entity } from '@client/reducers/users';
import service from '@client/services/users';
import * as sessionActions from '@client/actions/pages/sessions';
import { push } from '@client/actions/router';
import uuid from 'uuid/v1';
/* eslint-disable */
const { create, ...base } = baseActions('users', entity, service);
const er = erActions('users');
/* eslint-enable */
module.exports = {
  ...base,
  create: values => dispatch => {
    return service.create(values).then(({ session, user }) => {
      dispatch(sessionActions.login(session, { ...user, ...values }));
    });
  },
  goToUser: id => push(`/users/${id}`),
  goToUserEdit: id => push(`/users/${id}/edit`),
  getDiscourses: (id)=>(dispatch)=>{
    return Promise.resolve([{ id: uuid(), company: {
      id: uuid(),
    }}]).then((discourses)=>{
      dispatch(er.get({id, discourses}));
    })
  },
};
