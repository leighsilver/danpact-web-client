// @flow
import React, { Component } from 'react';
import { Map } from 'immutable';
import Input from 'react-toolbox/lib/input';

import { debounce } from 'lodash';

const noop = () => {};
type $props = {
  name?: string,
  error: string,
  onChange: () => void,
  value?: any,
  label?: string,
  placeholder?: string,
  multi?: boolean,
  readonly?: boolean,
  onEnter?: Function,
  isDirty?: boolean,
  initialValue?: any,
  isValid?: boolean,
  verify?: Function,
  onKeyPress?: Function,
  debounce?: boolean | number,
  disabled?: boolean,
  type?: string,
};

export class TextInput extends Component<$props> {
  input: any;
  onChange: Function;
  constructor(props: $props) {
    super(props);
    if (props.debounce) {
      this.onChange = debounce(
        this.onRegularChange,
        typeof props.debounce === 'number' ? props.debounce : 500
      );
    } else {
      this.onChange = this.onRegularChange;
    }
  }
  shouldComponentUpdate(nextProps: $props) {
    return (
      this.props.value !== nextProps.value ||
      this.props.error !== nextProps.error ||
      this.props.disabled !== nextProps.disabled
    );
  }
  // setInput = (input: any) => {
  //   this.input = input;
  // };
  // getInput = () => {
  //   return this.input;
  // };
  // handleOnEnter = (event: Object) => {
  //   if (event.key === 'Enter' && this.props.onEnter) {
  //     this.props.onEnter(event.target.value);
  //   }
  // };
  // handleKeyPress = (event: Object) => {
  //   if (this.props.onEnter) {
  //     this.handleOnEnter(event);
  //   }
  //   if (this.props.onKeyPress) this.props.onKeyPress(event);
  // };
  render() {
    const {
      label,
      type,
      placeholder,
      onChange,
      multi,
      readonly,
      isDirty,
      initialValue,
      isValid,
      verify,
      value,
      debounce,
      ...props
    } = this.props;
    return (
      <Input
        // ref={this.setInput}
        type={type}
        label={label}
        multiline={multi}
        placeholder={placeholder}
        // onKeyPress={multi ? undefined : this.handleKeyPress}
        defaultValue={value}
        disabled={readonly}
        onChange={this.onChange}
        {...props}
      />
    );
  }
  onRegularChange = (value: any) => {
    const { onChange = noop } = this.props;
    onChange(value);
  };
}

export default TextInput;

