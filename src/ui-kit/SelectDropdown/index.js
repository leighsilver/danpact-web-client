// @flow
import * as React from 'react';
import Async from 'react-select/lib/Async';
import AsyncCreatable from 'react-select/lib/AsyncCreatable';
import Creatable from 'react-select/lib/Creatable';
import BaseSelect from 'react-select/lib/Select';
import classnames from 'classnames';
import 'react-select/dist/react-select.css';
import styles from './style.pcss';
import './style.css';

type $props = {
  create: Boolean,
  className?: string,
  renderValues: (values: Array<any>) => any,
  onChange?: (values: Array<any>) => any,
  onAdd?: (value: any) => any,
  loadOptions: (input: string) => Promise<Array<Object>>,
  value: Array<any>,
  error: string,
  label: string,
  source?: Array<any>,
  sync?: boolean,
  valueKey?: string,
  labelKey?: string,
};

export default class SelectDropdown extends React.Component<$props> {
  onChange = (option: Object | Array<Object>) => {
    if(this.props.onBlur){
      this.props.onBlur();
    }
    if (this.props.multi){
      if (!option) {
        if(this.props.onChange)
          this.props.onChange([]);
      } else {
        if (this.props.onChange && Array.isArray(option))
          this.props.onChange(option.map(o=>o.id));
        if (this.props.onAdd) this.props.onAdd(option[option.length - 1].id);
      }
    } else {
      if (!option) {
        if(this.props.onChange)
          this.props.onChange(null);
      } else {
        if (this.props.onChange)
          this.props.onChange(option.id);
        if (this.props.onAdd) this.props.onAdd(option.id);
      }
    }
  };
  loadOptions = (input: string) => {
    const maybePromise = this.props.loadOptions(input);
    if(maybePromise && maybePromise.then){
      return maybePromise.then(options => {
        return {
          options
        };
      });
    }
    return maybePromise;
  };
  render() {
    const {
      create,
      renderValues,
      value,
      error,
      label,
      sync,
      className,
      valueKey,
      labelKey,
      ...props
    } = this.props;
    const Select = sync
      ? create ? Creatable : BaseSelect
      : create ? AsyncCreatable : Async;

      const otherProps = renderValues ? {
        ...props,
      } : {
        ...props,
        value,
      }
    console.log('error', error);
    return (
      <div className={styles.container}>
        {
          (value !== undefined && value !== null && value !== '') && <label className={styles.label}>{label}</label>
        }
        <Select
          placeholder={label}
          className={classnames('customReactSelect-a2z7', className)}
          valueKey={valueKey || "id"}
          labelKey={labelKey || "name"}
          {...otherProps}
          onChange={this.onChange}
          loadOptions={this.loadOptions}
        />
        {renderValues && renderValues(value)}
        {error && <div className={styles.error}>{error}</div>}
      </div>
    );
  }
}
