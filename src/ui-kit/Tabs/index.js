// @flow
import * as React from 'react';
import { Tabs as RTTabs } from 'react-toolbox';

type $props = Object;

const state = {
  index: 0
};

export default class Tabs extends React.Component<$props, typeof state> {
  state = state;
  componentWillReceiveProps(nextProps: $props){
    if(nextProps.index !== this.props.index)
      this.setState({
        index: nextProps.index
      })
  }
  constructor(props: $props) {
    super(props);
    if (props.index) {
      this.state = {
        index: props.index
      };
    }
  }
  handleTabChange = (index: number) => {
    this.props.onTabChange ? this.props.onTabChange(index) : this.setState({
      index
    });
  };
  render() {
    return (
      <RTTabs
        {...this.props}
        index={this.state.index}
        onChange={this.handleTabChange}
      >
        {this.props.children}
      </RTTabs>
    );
  }
}
