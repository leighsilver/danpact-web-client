// @flow
import * as React from 'react';
import RTCheckbox from 'react-toolbox/lib/checkbox';

export default class Checkbox extends React.PureComponent <*> {
  render(){
    const { value, ...props } = this.props;
    return <RTCheckbox  checked={value} {...props}/>
  }
}
