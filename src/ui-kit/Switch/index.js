// @flow
import * as React from 'react';
import RTSwitch from 'react-toolbox/lib/switch';

type $props = {
  value: boolean,
  onChange: Function,
  label: string,
}

export default class Switch extends React.PureComponent <$props> {
  render(){
    const { value, ...props } = this.props;
    return <RTSwitch checked={value} {...props} />;
  }
}