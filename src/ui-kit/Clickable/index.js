// @flow
import * as React from 'react';
import classnames from 'classnames';
import styles from './style.pcss';
export default class Clickable extends React.PureComponent<*, *> {
  render(){
    const { className, ...props } = this.props;
    return <span {...props} className={classnames(styles.span, className)} />
  }
}