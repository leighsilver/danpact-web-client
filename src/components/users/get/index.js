// @flow

import khange, { kheck } from 'khange';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { flowRight } from 'lodash';
import User from '@client/models/User';
import * as userSelectors from '@client/selectors/users';
import { get } from '@client/actions/users';
import { bindActionCreators } from '@client/utils/components';
import { formParent } from '@client/hocs';


export type $stateProps = {
  id: $$id,
  user: User
};

type $dispatchProps = {
  get: Function,
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  id: userSelectors.currentUserId,
  user: userSelectors.currentUser,
});

export const mapDispatchToProps = (dispatch: $$dispatch): $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      get
    },
    dispatch
  );

  export const onKhange = (props: $$formParentProps & $stateProps & $dispatchProps) => {
  if(!props.id){
    return null;
  }
  props.get(props.id).then(() => {
    if (props.reinitializeForm) props.reinitializeForm.go();
  });
};

export default flowRight([
  formParent,
  connect(mapStateToProps, mapDispatchToProps),
  khange(kheck('id'), onKhange)
])