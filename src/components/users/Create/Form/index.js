// @flow
import React from 'react';
import { TextInput, Button } from 'ui-kit';
import { flowRight } from 'lodash';
import { create } from '@client/actions/users';
import { connect } from 'react-redux';
import form from '@client/hocs/form';
import { goHome } from '@client/actions/pages/homes';
import Yup from 'yup';
import uuid from 'uuid/v1';

type $props = Object;

export class CreateUser extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <TextInput type="email" {...props.fields.email} />
        <TextInput {...props.fields.fullName} />
        <TextInput {...props.fields.location} />
        {JSON.stringify(props.errors)}
        <Button
          raised
          primary
          disabled={!this.props.isValid}
          onClick={this.props.handleSubmit}
        >
          Signup
        </Button>
      </div>
    );
  }
}

const formik = form({
  mapPropsToValues: () => ({ email: '', fullName: '', location: '' }),
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email('Invalid email address')
      .required('Email is required!'),
    fullName: Yup.string().required(),
    location: Yup.string().required(),
  }),
  handleSubmit: (values, { props }) => {
    const id = uuid();
    props.goHome();
    return props
      .create({
        ...values,
        id,
        talent: props.talent,
      });
  }
});

export const mapDispatchToProps = (dispatch: $$dispatch) => ({
  create: (values: Object) => dispatch(create(values)),
  goHome: () => dispatch(goHome())
});

export default flowRight([connect(null, mapDispatchToProps), formik])(
  CreateUser
);
