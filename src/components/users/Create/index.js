// @flow
import * as React from 'react';
import Form from './Form';
import Tabs from 'ui-kit/Tabs';
import Tab from 'ui-kit/Tabs/Tab';

export default class CreateUser extends React.PureComponent<*> {
  render(){
    return <Tabs>
      <Tab label="Talent">
        <Form talent />
      </Tab>
      <Tab label="Employer">
        <Form talent={false} />
      </Tab>
    </Tabs>
  }
}