// @flow
import * as React from 'react';
import { Tabs, Tab } from 'ui-kit';
import MyTests from 'components/tests/My';
import { flowRight } from 'lodash';
import { currentUser } from '@client/selectors/users';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

type $props = Object;

export class UserTabs extends React.PureComponent<$props> {
  render() {
    const { canEdit, userId } = this.props;
    return (
      <Tabs fixed>
        <Tab label="Tests">
          <MyTests userId={userId} canEdit={canEdit} />
        </Tab>
      </Tabs>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  currentUser
});

export default flowRight([connect(mapStateToProps)])(UserTabs);
