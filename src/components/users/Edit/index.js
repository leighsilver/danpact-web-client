// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';
import { bindActionCreators } from '@client/utils/components';
import { connect } from 'react-redux';
import { update } from '@client/actions/users';
import { Card, CardText, TextInput } from 'ui-kit';
import get from '../get';
import Locations from 'components/general/form/Locations';
import { validationSchema } from '@client/utils/users';

type $props = Object;

export class UserEdit extends React.PureComponent<$props> {
  render() {
    const { fields, ...props } = this.props;
    console.log('fields', fields.city);
    return (
      <div>
          <TextInput debounce {...fields.fullName} />
          <Locations {...fields.city} />
        </div>
    );
  }
}

const formik = {
  mapPropsToValues: ({ user }) => {
    const { fullName, city } = user.toObject();
    return {
      fullName,
      city,
    };
  },
  validationSchema,
  handleChange: props => {
    return (key, onChange) => {
      return value => {
        onChange(value);
        return props.update(props.id, {
          [key]: value
        });
      };
    };
  }
};

const mapDispatchToProps = (dispatch: $$dispatch) =>
  bindActionCreators(
    {
      update
    },
    dispatch
  );

export default flowRight([
  get,
  connect(null, mapDispatchToProps),
  form(formik)])(
  UserEdit
);
