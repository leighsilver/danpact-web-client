// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput, MultiDropdown } from 'ui-kit';
import { connect } from 'react-redux';
import { SelectDropdown } from '../../../ui-kit';

import { bindActionCreators } from '../../../@client/utils/components';


type $props = Object;

export class PrescriptionForm extends React.PureComponent<$props> {
  renderBodyLevels = (values: Array<$$id>) => {
    return <div />
  };
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <TextInput {...props.fields.name} />
        <TextInput rows={3} multiline {...props.fields.notes} />
        <SelectDropdown
          renderValues={this.renderBodyLevels}
          loadOptions={this.props.search}
          {...props.fields.bodyLevels}
        />
        <MultiDropdown {...props.fields.refs} label="References" source={[]} />
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch: $$dispatch) =>
  bindActionCreators(
    {
    },
    dispatch
  );

export default flowRight([connect(null, mapDispatchToProps)])(PrescriptionForm);
