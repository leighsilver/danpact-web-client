// @flow
import React from 'react';
import { UL } from 'ui-kit';
import { flowRight } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import TestItem from '../Item';

type $props = Object;

export class ShowAllTests extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getTests();
  }
  render() {
    return (
      <div>
        <UL>
          {this.props.testIds.map(id => {
            return <TestItem key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) =>
  bindActionCreators(
    {
    },
    dispatch
  );

export const mapStateToProps = createStructuredSelector({
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  ShowAllTests
);
