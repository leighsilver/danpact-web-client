// @flow

import khange, { kheck } from 'khange';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getParam } from '@client/selectors/router';
import { flowRight } from 'lodash';
import * as companySelectors from '@client/selectors/companies';
import Company from '@client/models/Company'
import { get } from '@client/actions/companies';
import { bindActionCreators } from '@client/utils/components';
import { formParent } from '@client/hocs';

const getCompanyId = getParam('companyId');

export type $stateProps = {
  id: $$id,
  company: Company,
};

type $dispatchProps = {
  get: Function,
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  id: getCompanyId,
  company: companySelectors.find(getCompanyId)
});

export const mapDispatchToProps = (dispatch: $$dispatch): $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      get
    },
    dispatch
  );

  export const onKhange = (props: $$formParentProps & $stateProps & $dispatchProps) => {
  props.get(props.id).then(() => {
    if (props.reinitializeForm) props.reinitializeForm.go();
  });
};

export default flowRight([
  formParent,
  connect(mapStateToProps, mapDispatchToProps),
  khange(kheck('id'), onKhange)
])