// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { update as updateCompany } from '@client/actions/companies';
import { generalValidationSchema as validationSchema } from '@client/utils/companies';
import CompanyForm from './Form';
import type { $stateProps as $getCompanyProps} from '../../get'

type $stateProps = {};
type $dispatchProps = {
  updateCompany: Function,
};
type $ownProps = $getCompanyProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class GeneralCompany extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <CompanyForm fields={this.props.fields} />
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {
      updateCompany: values => updateCompany(id, values),
    },
    dispatch
  );

export default flowRight([
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: ({ company }) => ({
      name: company.name,
      size: company.size,
    }),
    validationSchema,
    handleChange: props => {
      return (key, onChange) => {
        return value => {
          onChange(value);
          return props.updateCompany({ [key]: value });
        };
      };
    },
  }),
])(GeneralCompany);
