// @flow

import React from 'react';
import { flowRight } from 'lodash';
import CompanyGeneral from './General';
import CompanyPreferences from './Preferences';
import getCompany from '../get';
import Tabs from 'ui-kit/Tabs';
import Tab from 'ui-kit/Tabs/Tab';

import type { $stateProps as $getCompanyProps} from '../get'

type $stateProps = {};
type $dispatchProps = {
  updateCompany: Function,
};
type $ownProps = $getCompanyProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class EditCompany extends React.PureComponent<$props> {
  render() {
    return (
      <div>
        <h3>Edit Company</h3>
        <Tabs>
          <Tab label="Preferences">
            <CompanyPreferences {...this.props} />
          </Tab>
          <Tab label="General">
            <CompanyGeneral {...this.props} />
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default flowRight([
  getCompany,
])(EditCompany);
