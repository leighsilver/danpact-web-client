// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput } from 'ui-kit';
import CompanySize from 'components/general/form/CompanySize';

type $props = {
  fields: Object;
};

export class PreferencesForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <TextInput {...props.fields.name} />
        <CompanySize {...props.fields.companySize} label="How many employees?" />
      </React.Fragment>
    );
  }
}

export default flowRight()(PreferencesForm);
