// @flow
import React from 'react';
import { UL } from 'ui-kit';
import { flowRight } from 'lodash';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getCompanies } from '@client/actions/pages/homes';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/pages/homes';
import CompanyItem from '../Item';

type $stateProps = {
  companyIds: List<$$id>
};
type $dispatchProps = {
  getCompanies: Function,
};
type $ownProps = {

};

type $props = $stateProps & $dispatchProps & $ownProps;

export class ShowAllCompanys extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getCompanies();
  }
  render() {
    return (
      <div>
        <UL>
          {this.props.companyIds.map(id => {
            return <CompanyItem key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      getCompanies
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  companyIds: getRelated('companies')
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  ShowAllCompanys
);
