// @flow
import * as React from 'react'
import { flowRight, bindActionCreators } from '@client/utils/components'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import Company from '@client/models/Company';
import { find } from '@client/selectors/companies';

type $stateProps = {company: Company}
type $ownProps = {}
type $dispatchProps = {}
type $props = $stateProps & $dispatchProps & $ownProps
export class  CompanyBreakdown extends React.PureComponent<$props> {
  render(){
    const { ...props } = this.props
    return  <div>
      <h3>{props.company.name}</h3>
    </div>
  }
}

const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  company: find()
});

const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> => bindActionCreators({
}, dispatch)

export default flowRight([
  connect(mapStateToProps, mapDispatchToProps)
])( CompanyBreakdown)