// @flow
import React from 'react';
import { UL, ULItem } from 'ui-kit';
import { flowRight } from 'lodash';
import { push } from '@client/actions/router';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getCompanies } from '@client/actions/users';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/users';
import CompanyItem from '../Item';

type $stateProps = {
  companyIds: List<$$id>
};
type $dispatchProps = {
  goToCreateCompany: Function,
  getCompanies: Function,
};
type $ownProps = {
  canEdit: boolean,
  userId: $$id,
};

type $props = $stateProps & $dispatchProps & $ownProps;

export class MyCompanys extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getCompanies();
  }
  render() {
    const { canEdit } = this.props;
    return (
      <div>
        <UL>
          {canEdit && (
            <ULItem
              onClick={this.props.goToCreateCompany}
              selectable
              caption="Create Company"
              leftIcon="add"
            />
          )}
          {this.props.companyIds.map(id => {
            return <CompanyItem canEdit={canEdit} key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, props: $ownProps) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToCreateCompany: () => push('/companies/create'),
      getCompanies: ()=>getCompanies(props.userId)
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  companyIds: getRelated('companies', (state, props)=>props.userId)
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  MyCompanys
);
