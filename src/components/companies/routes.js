// @flow
import * as React from 'react';
import { Route } from 'react-router';
import ShowAll from './ShowAll';
// import Create from './Create';
import Show from './Show';
import Edit from './Edit';

export default [
  <Route exact key="ShowAll" path="/companies" component={ShowAll} />,
  // <Route exact key="Create" path="/companies/create" component={Create} />,
  <Route exact key="Edit" path="/companies/:companyId/edit" component={Edit} />,
  <Route exact key="Show" path="/companies/:companyId" component={Show} />,
]
