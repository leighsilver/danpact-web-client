// @flow
import * as React from 'react';
import { ULItem } from 'ui-kit';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import * as companySelectors from '@client/selectors/companies';
import { goToEditCompany, goToCompany } from '@client/actions/companies';
import { bindActionCreators } from '@client/utils/components';
import Company from '@client/models/Company'

type $stateProps = {
  company: Company
};

type $ownProps = {
  id: $$id,
  canEdit?: boolean,
};

type $dispatchProps = {
  goToEditCompany: Function,
  goToCompany: Function,
};

type $props = $ownProps & $stateProps & $dispatchProps;

export class CompanyItem extends React.PureComponent<$props> {
  render() {
    const { canEdit, company, goToEditCompany, goToCompany, ...props } = this.props;
    return (
      <ULItem
        {...props}
        onClick={canEdit ? goToEditCompany : goToCompany}
        caption={company.name}
      />
    );
  }
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  company: companySelectors.find()
});

export const mapDispatchToProps = (dispatch: $$dispatch, props: $props) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToEditCompany: () => goToEditCompany(props.id),
      goToCompany: () => goToCompany(props.id)
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CompanyItem);
