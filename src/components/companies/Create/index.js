// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';

import { Button } from 'ui-kit';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { validationSchema } from '@client/utils/companies';
import { create as createCompany, goToCompany } from '@client/actions/companies';
import uuid from 'uuid/v1';
import CompanyForm from '../Form';

type $ownProps = {
  
}

type $stateProps = {

}

type $dispatchProps = {
  createCompany: Function,
  goToCompany: Function,
}

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class CreateCompany extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>Create Company</h1>
        <CompanyForm fields={this.props.fields} />
        <Button
          primary
          disabled={!props.isValid}
          onClick={this.props.handleSubmit}
        >
          Create
        </Button>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      createCompany,
      goToCompany
    },
    dispatch
  );

export default flowRight([
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: () => ({
      name: '',
      notes: '',
      instructives: '',
      refs: []
    }),
    validationSchema,
    handleSubmit: (values, { props }) => {
      const id = uuid();
      props.goToCompany(id);
      return props.createCompany({ ...values, id });
    }
  })
])(CreateCompany);
