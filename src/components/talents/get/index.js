// @flow

import khange, { kheck } from 'khange';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { flowRight } from 'lodash';
import User from '@client/models/User';
import * as talentSelectors from '@client/selectors/talents';
import * as userSelectors from '@client/selectors/users';
import Talent from '@client/models/Talent'
import { get } from '@client/actions/talents';
import { bindActionCreators } from '@client/utils/components';
import { formParent } from '@client/hocs';

const getTalentId = userSelectors.currentUserId;

export type $stateProps = {
  id: $$id,
  talent: Talent,
  user: User
};

type $dispatchProps = {
  get: Function,
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  id: getTalentId,
  talent: talentSelectors.find(getTalentId),
  user: userSelectors.currentUser,
});

export const mapDispatchToProps = (dispatch: $$dispatch): $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      get
    },
    dispatch
  );

  export const onKhange = (props: $$formParentProps & $stateProps & $dispatchProps) => {
  if(!props.id){
    return null;
  }
  props.get(props.id).then(() => {
    if (props.reinitializeForm) props.reinitializeForm.go();
  });
};

export default flowRight([
  formParent,
  connect(mapStateToProps, mapDispatchToProps),
  khange(kheck('id'), onKhange)
])