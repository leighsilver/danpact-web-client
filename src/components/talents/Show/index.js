// @flow
import React from 'react';
import { flowRight } from 'lodash';
import ULItem from 'ui-kit/UL/Item';
import UL from 'ui-kit/UL';
import Button from 'ui-kit/Button';
import getTalent from '../get';
import type { $stateProps } from '../get';
import CreateDiscourse from 'components/discourses/Create';
import TextInput from 'ui-kit/TextInput';

type $props = $stateProps;

class ShowText extends React.PureComponent<Object> {
  render() {
    const { props } = this;
    return (
      <div>
        <h3>{props.title}</h3>
        <p>{props.value}</p>
      </div>
    );
  }
}

class ShowMulti extends React.PureComponent<Object> {
  render() {
    const { props } = this;
    return (
      <div>
        <h3>{props.title}</h3>
        <UL>
          {props.values.map((value, i) => {
            return <ULItem key={i} caption={value} />;
          })}
        </UL>
      </div>
    );
  }
}

export class ShowTalent extends React.PureComponent<$props> {
  state = {
    interviewRequest: false
  }
  toggleInterviewRequest = () => {
    this.setState({
      interviewRequest: !this.state.interviewRequest,
    });
  }
  render() {
    const { props } = this;
    return (
      <div>
        {
          this.state.interviewRequest ? <CreateDiscourse talentId={props.talent.id} /> : <Button onClick={this.toggleInterviewRequest}>Open Interview Request</Button>
        }
        <h1>{props.user.fullName}</h1>
        
      </div>
    );
  }
}

export default flowRight([getTalent])(ShowTalent);
