// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';

import { Button } from 'ui-kit';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { validationSchema } from '@client/utils/talents';
import { create as createTalent, goToTalent } from '@client/actions/talents';
import uuid from 'uuid/v1';
import TalentForm from '../Form';

type $ownProps = {
  
}

type $stateProps = {

}

type $dispatchProps = {
  createTalent: Function,
  goToTalent: Function,
}

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class CreateTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>Create Talent</h1>
        <TalentForm fields={this.props.fields} />
        <Button
          primary
          disabled={!props.isValid}
          onClick={this.props.handleSubmit}
        >
          Create
        </Button>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      createTalent,
      goToTalent
    },
    dispatch
  );

export default flowRight([
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: () => ({
      name: '',
      notes: '',
      instructives: '',
      refs: []
    }),
    validationSchema,
    handleSubmit: (values, { props }) => {
      const id = uuid();
      props.goToTalent(id);
      return props.createTalent({ ...values, id });
    }
  })
])(CreateTalent);
