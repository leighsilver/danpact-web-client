// @flow
import React from 'react';
import { UL } from 'ui-kit';
import { flowRight } from 'lodash';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getTalents } from '@client/actions/pages/homes';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/pages/homes';
import TalentItem from '../Item';

type $stateProps = {
  talentIds: List<$$id>
};
type $dispatchProps = {
  getTalents: Function,
};
type $ownProps = {

};

type $props = $stateProps & $dispatchProps & $ownProps;

export class ShowAllTalents extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getTalents();
  }
  render() {
    return (
      <div>
        <UL>
          {this.props.talentIds.map(id => {
            return <TalentItem key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      getTalents
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  talentIds: getRelated('talents')
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  ShowAllTalents
);
