// @flow
import React from 'react';
import { UL, ULItem } from 'ui-kit';
import { flowRight } from 'lodash';
import { push } from '@client/actions/router';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getTalents } from '@client/actions/users';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/users';
import TalentItem from '../Item';

type $stateProps = {
  talentIds: List<$$id>
};
type $dispatchProps = {
  goToCreateTalent: Function,
  getTalents: Function,
};
type $ownProps = {
  canEdit: boolean,
  userId: $$id,
};

type $props = $stateProps & $dispatchProps & $ownProps;

export class MyTalents extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getTalents();
  }
  render() {
    const { canEdit } = this.props;
    return (
      <div>
        <UL>
          {canEdit && (
            <ULItem
              onClick={this.props.goToCreateTalent}
              selectable
              caption="Create Talent"
              leftIcon="add"
            />
          )}
          {this.props.talentIds.map(id => {
            return <TalentItem canEdit={canEdit} key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, props: $ownProps) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToCreateTalent: () => push('/talents/create'),
      getTalents: ()=>getTalents(props.userId)
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  talentIds: getRelated('talents', (state, props)=>props.userId)
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  MyTalents
);
