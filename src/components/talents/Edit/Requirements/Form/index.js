// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { SelectDropdown } from 'ui-kit';
import { connect } from 'react-redux';
import Compensation from 'components/talents/Edit/Requirements/Form/Compensation';

type $props = {
  fields: Object;
};

const careerAdvancedmentOpportunitiesOptions = [
  { id: 1, name: 'Leadership and management opportunities' },
  { id: 2, name: 'Professional education' },
];

const overtimeOptions = [
  { id: 1, name: 'Timeoff-in-lieu' },
  { id: 2, name: 'Paid' },
  { id: 3, name: 'Highest possible salary, overtime policy is not important' },
];

const weeklyHoursOptions = [
  {
    id: 1, name: '35 hours',
  },{
    id: 2, name: '40 hours',
  },{
    id: 3, name: '45 hours',
  },{
    id: 4, name: 'Highest salary, no weekly hours preference',
  },
]

export class RequirementsForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <Compensation {...props} />
        <SelectDropdown sync {...props.fields.advancement} label={"What sort of career advancement opportunities are you interested in?"} options={careerAdvancedmentOpportunitiesOptions}/>
        <SelectDropdown sync {...props.fields.overtime} label={"Which overtime policy do you prefer?"} options={overtimeOptions}/>
        <SelectDropdown sync {...props.fields.weeklyHours} label={"How many hours do you want to work each week?"} options={weeklyHoursOptions}/>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch)=>({
  search: (text)=>{
    return Promise.resolve([{ id: 'Toronto, ON CA', name: 'Toronto, ON, CA'}]);
  }
})

export default flowRight([
  connect(null, mapDispatchToProps)
])(RequirementsForm);
