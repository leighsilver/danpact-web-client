// @flow
import * as React from 'react'
import TextInput from 'ui-kit/TextInput';
import Row from 'ui-kit/Row';
import Col from 'ui-kit/Col';
import SelectDropdown from 'ui-kit/SelectDropdown';

const vacationOptions = [
  {
    id: 1, name: '6 weeks',
  },
  {
    id: 2, name: '8 weeks',
  },
  {
    id: 3, name: '10 weeks',
  },
  {
    id: 4, name: '12 weeks',
  },
  {
    id: 5, name: '14 weeks',
  },
  {
    id: 6, name: '16 weeks',
  }
]

const danpactOptions = [
  {
    id: '', name: 'Choose Contract', compensation: 'Living Cost Adjusted Compensation', vacation: 'Paid Leave'
  },
  {
    id: 1, name: 'White Contract (1)', compensation: '$50,000+', vacation: '6 weeks+ vacation', number: 50000, vacationId: 1,
    
  },
  {
    id: 2, name: 'Blue Contract (2)', compensation: '$60,000+', vacation: '8 weeks+ vacation', number: 60000, vacationId: 2,
  },
  {
    id: 3, name: 'Purple Contract (3)', compensation: '$70,000+', vacation: '10 weeks+ vacation', number: 70000, vacationId: 3,
  },
  {
    id: 4, name: 'Brown Contract (4)', compensation: '$80,000+', vacation: '12 weeks+ vacation', number: 80000, vacationId: 4,
  },
  {
    id: 5, name: 'Red Contract (5)', compensation: '$90,000+', vacation: '14 weeks+ vacation', number: 90000, vacationId: 5,
  },
  {
    id: 6, name: 'Black Contract (6)', compensation: '$100,000+', vacation: '16 weeks+ vacation', number: 100000, vacationId: 6,
  },
]

type $props = Object

const state = {
  danpact: '',
};

const getDanpact = (compensation, vacation)=>{
  let i = danpactOptions.length - 1;
  let danpact;
  while(i > 0 && danpact === undefined){
    const option = danpactOptions[i];
    if(option.number <= compensation && option.vacationId <= vacation)
      danpact = option.id;
    i--
  }
  if (danpact) {
    return danpact;
  }
  return '';
}

      

export default class Compensation extends React.PureComponent<$props, typeof state> {
  state = state;
  componentWillReceiveProps(nextProps: $props){
    if(nextProps.fields.compensation.value !== this.props.fields.compensation.value || nextProps.fields.vacation.value !== this.props.fields.vacation.value){
      const danpact = getDanpact(nextProps.fields.compensation.value, nextProps.fields.vacation.value)
      this.setState({
        danpact,
      })
    }
  }
  constructor(props){
    super(props);
    if(props.fields.compensation.value || props.fields.vacation.value){
      const danpact = getDanpact(props.fields.compensation.value, props.fields.vacation.value)
      this.state = {
        danpact
      }
    }
  }
  onChangeDanpact = (danpact) => {
    const option = danpactOptions.find((d)=>d.id === danpact);
    this.setState({
      danpact: danpact || '',
    });
    return this.props.fields.compensation.onChange(option.number).then(()=>{
      return this.props.fields.vacation.onChange(option.vacationId);
    })
  }
  render(){
    const { ...props } = this.props
    const selectedDanpact = danpactOptions.find((d)=>{
      return d.id === this.state.danpact;
    })
    return  <React.Fragment>
      <h4>Salary and Vacation</h4>
      <Row>
        <Col xs={6}>
          <SelectDropdown onChange={this.onChangeDanpact} value={this.state.danpact} sync options={danpactOptions} />
        </Col>
        <Col xs={6}>
          <p>{`${selectedDanpact.compensation}, ${selectedDanpact.vacation}`}</p>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <TextInput debounce key={this.state.danpact} icon={<span>$</span>} {...props.fields.compensation} label="Minimum Compensation" type="number" />
        </Col>
        <Col xs={6}>
          <SelectDropdown {...props.fields.vacation} sync options={vacationOptions} label="Minimum Vacation" />
        </Col>
      </Row>
    </React.Fragment>
  }
}