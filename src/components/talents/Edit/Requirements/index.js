// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { requirementsValidationSchema as validationSchema } from '@client/utils/talents';
import RequirementsForm from './Form';
import type { $stateProps as $getTalentProps} from '../../get'

type $stateProps = {};
type $dispatchProps = {
};
type $ownProps = $getTalentProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class PreferencesTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <RequirementsForm fields={props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {
    },
    dispatch
  );

export const form = {
  mapPropsToValues: ({ talent }) => ({
    advancement: talent.advancement,
    compensation: talent.compensation,
    vacation: talent.vacation,
    overtime: talent.overtime,
    weeklyHours: talent.weeklyHours,
  }),
  validationSchema,
  handleChange: props => {
    return (key, onChange) => {
      return value => {
        onChange(value);
        return props.updateTalent({ [key]: value });
      };
    };
  },
};

export default flowRight([
  connect(null, mapDispatchToProps),
])(PreferencesTalent);
