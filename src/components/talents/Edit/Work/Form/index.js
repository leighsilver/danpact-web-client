// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { Button } from 'ui-kit';
import Position from './Position';
import uuid from 'uuid/v1';
import { connect } from 'react-redux';
import { updateLocal } from '@client/actions/talents';
import * as talentServices from '@client/services/talents';
import { List } from 'immutable';
import { bindActionCreators } from 'redux';
import UL from 'ui-kit/UL';
import ULItem from 'ui-kit/UL/Item';
import Clickable from 'ui-kit/Clickable';
type $ownProps = {
  id: $$id;
  fields: Object;
  updateTalent: Function;
  work: List<Object>;
  reinitializeForm: Object;
};

type $dispatchProps = {
  updateTalentLocal: Function,
}

type $props = $ownProps & $dispatchProps;

export class WorkForm extends React.PureComponent<$props> {
  addPosition = () => {
    this.props.updateTalent({ work: this.props.work.toJS().concat([{id: uuid()}])}).then(()=>{
      this.props.reinitializeForm.workForm();
    })
  }
  deletePosition = (index: number) => {
    console.log('index');
    const work = this.props.work.toJS()
    work.splice(index, 1);
    this.props.updateTalentLocal({ work })
    this.props.reinitializeForm.workForm();
    talentServices.update(this.props.id, { work  });
  }
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <UL>
          {
            props.fields.map((positionFields, index) => {
              return <ULItem
                ripple={false}
                key={index} 
                content={
                  <Position updateTalent={this.props.updateTalent} fields={positionFields} />
                }
                rightActions={[
                  <Clickable key="delete" onClick={this.deletePosition.bind(this, index)}>x</Clickable>
                ]}
              />
            })
          }
        </UL>
        <Button onClick={this.addPosition}>+</Button>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps : $$selectorExact<$dispatchProps> = (dispatch, props)=>bindActionCreators({
  updateTalentLocal: (values)=>updateLocal(props.id, values),
}, dispatch)


export default flowRight([
  connect(null, mapDispatchToProps)
])(WorkForm);
