// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput, Row, Col, Checkbox } from 'ui-kit';

type $props = {
  fields: Object;
};

export class PositionForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <Row>
          <Col xs={6}>
            <TextInput debounce {...props.fields.company} />
          </Col>
          <Col xs={6}>
            <TextInput debounce {...props.fields.position} />
          </Col>
        </Row>
        <Row>
          <Col xs={3}>
            <TextInput debounce type="number" {...props.fields.startMonth} />
          </Col>
          <Col xs={3}>
            <TextInput debounce type="number" {...props.fields.startYear} />
          </Col>
          <Col xs={3}>
            <TextInput debounce type="number" disabled={this.props.fields.current.value} {...props.fields.endMonth} />
          </Col>
          <Col xs={3}>
            <TextInput debounce type="number" disabled={this.props.fields.current.value} {...props.fields.endYear} />
          </Col>

        </Row>
        <Row><Col xs={12}><Checkbox {...props.fields.current} /></Col></Row>
        <Row><Col xs={12}><TextInput debounce {...props.fields.description} multiline/></Col></Row>
      </React.Fragment>
    );
  }
}

export default flowRight()(PositionForm);
