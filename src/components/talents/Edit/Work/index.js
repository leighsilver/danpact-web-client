// @flow

import React from 'react';
import { flowRight } from 'lodash';
import WorkForm from './Form';
import type { $stateProps as $getTalentProps} from '../../get'
import Yup from 'yup';
import { rawPositionValidationSchema } from '@client/utils/talents';
import { getArrayValidationSchema, getMapPropsToValues } from '@client/hocs/form';

type $stateProps = {};
type $dispatchProps = {
};
type $ownProps = $getTalentProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps & $$formParentProps;

export class WorkTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <WorkForm reinitializeForm={this.props.reinitializeForm} id={props.id} fields={props.fields.arrayFields.work} work={this.props.talent.work} updateTalent={props.updateTalent} />
    );
  }
}

const mapPropsToValues = ({talent}) => ({work: talent.work.toJS()})
const arrayPropsObject =   {
  work: rawPositionValidationSchema
};

const validationSchema = getArrayValidationSchema(mapPropsToValues, arrayPropsObject, { work: Yup.array().min(1).required() });

export const form = {
  mapPropsToValues: getMapPropsToValues(mapPropsToValues, arrayPropsObject),
  validationSchema,
  array: arrayPropsObject,
  handleChange: props => {
    return (key, onChange, arrayProp, getNextArray) => {
      return value => {
        onChange(value);
        return props.updateTalent({ [arrayProp]: getNextArray(props.talent.work.toJS(), value) });
      };
    };
  },
};

export default flowRight([])(WorkTalent);
