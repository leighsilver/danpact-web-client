// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { preferencesValidationSchema as validationSchema } from '@client/utils/talents';
import PreferencesForm from './Form';
import type { $stateProps as $getTalentProps} from '../../get'

type $stateProps = {};
type $dispatchProps = {
};
type $ownProps = $getTalentProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class PreferencesTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <PreferencesForm fields={props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {
    },
    dispatch
  );

export const form = {
  mapPropsToValues: ({ talent }) => ({
    cities: talent.cities.toArray(),
    remote: talent.remote,
    companySize: talent.companySize,
    relocate: talent.relocate,
    advancement: talent.advancement,
    culture: talent.culture,
    diversity: talent.diversity,
    layout: talent.layout,
    games: talent.games,
    social: talent.social,
    homeWork: talent.homeWork,
  }),
  validationSchema,
  handleChange: props => {
    return (key, onChange) => {
      return value => {
        onChange(value);
        return props.updateTalent({ [key]: value });
      };
    };
  },
};

export default flowRight([
  connect(null, mapDispatchToProps),
])(PreferencesTalent);
