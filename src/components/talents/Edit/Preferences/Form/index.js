// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { SelectDropdown, Checkbox } from 'ui-kit';
import CompanySize from 'components/general/form/CompanySize';
import { connect } from 'react-redux';
import Locations from 'components/general/form/Locations';
import Row from 'ui-kit/Row';
import Col from 'ui-kit/Col';

type $props = {
  fields: Object;
};

const addNoPreference = (options)=>([{
  id: 0, name: 'No preference',
}].concat(options));

const remoteOptions = addNoPreference([
  {id: 1, name: 'Yes'},
  {id: 2, name: 'No'},
  {id: 3, name: 'Only'},
]);

const cultureOptions = addNoPreference([
  {id: 1, name: 'Team first'},
  {id: 2, name: 'Elite members'},
  {id: 3, name: 'Horizontal members'},
  {id: 4, name: 'Conventional'},
]);

const diversityOptions = addNoPreference([
  {id: 1, name: 'Sex, gender, and sexuality diversity'},
  {id: 2, name: 'Ethnic and religious diversity'},
  {id: 3, name: 'Age diversity'},
  {id: 4, name: 'Disability diversity'}
]);

const officeLayoutOptions = addNoPreference([
  {id: 1, name: 'Cubicles'},
  {id: 2, name: 'Open concept - long desk'},
  {id: 3, name: 'Open concept - private desk'},
  {id: 4, name: 'Private rooms'},
]);

const gameOptions = addNoPreference([
  {id: 1, name: 'Table-top (ie. table tennis)'},
  {id: 2, name: 'Board games / RPGs'},
  {id: 3, name: 'Outdoor recreation'},
  {id: 4, name: 'Computer/Console'}
]);

const socialOptions = addNoPreference([
  {id: 1, name: 'Weekly'},
  {id: 1, name: 'Biweekly'},
  {id: 2, name: 'Monthly'},
  {id: 3, name: 'Quarterly'},
]);

const homeworkOptions = addNoPreference([
  {id: 1, name: 'Restricted'},
  {id: 2, name: 'Once a month'},
  {id: 3, name: 'Once a week'},
  {id: 4, name: 'Unrestricted'},
]);

export class GeneralForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <Row>
          <Col xs={6}>
            <Locations multi {...props.fields.cities} label="I would like to work in these cities"/>
          </Col>
          <Col xs={6}>
            <Checkbox {...props.fields.relocate} label="Are you willing to relocate?"/>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
          <SelectDropdown sync {...props.fields.remote} label={"Are you interested in remote work?"} options={remoteOptions}/>
          </Col>
          <Col xs={6}>
          <CompanySize {...props.fields.companySize} label="Do you have a preferred company size?" />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <SelectDropdown sync {...props.fields.culture} label={"Are you interested in any company cultures?"} options={cultureOptions}/>
          </Col>
          <Col xs={6}>
          <SelectDropdown sync options={diversityOptions} {...props.fields.diversity} label="Do you have any diversity preferences?" />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
          <SelectDropdown sync options={officeLayoutOptions} {...props.fields.layout} label="Do you have any office layout preferences?" />
          </Col>
          <Col xs={6}>
          <SelectDropdown sync options={gameOptions} {...props.fields.games} label="Do you have a preference for games during work hours?" />
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <SelectDropdown sync options={socialOptions} {...props.fields.social} label="Do you have a preference for official non-work-hour company gatherings?" />          </Col>
          <Col xs={6}>
            <SelectDropdown sync options={homeworkOptions} {...props.fields.homeWork} label="Do you have a preferred work from home policy?" />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch)=>({
})

export default flowRight([
  connect(null, mapDispatchToProps)
])(GeneralForm);
