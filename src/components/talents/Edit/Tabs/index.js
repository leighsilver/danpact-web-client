// @flow
import * as React from 'react'
import { flowRight, bindActionCreators } from '@client/utils/components'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import Tabs from 'ui-kit/Tabs';
import Tab from 'ui-kit/Tabs/Tab';
import form from '@client/hocs/form';
import { update as updateTalent } from '@client/actions/talents';
import getTalent from 'components/talents/get';
import SettingsTalent, { form as settingsFormProperties} from 'components/talents/Edit/Settings';
import JobTalent, {form as jobFormProperties } from 'components/talents/Edit/Job';
import LinksTalent, {form as linksFormProperties} from 'components/talents/Edit/Links';
import WorkTalent, {form as workFormProperties} from 'components/talents/Edit/Work';
import EducationTalent, {form as educationFormProperties} from 'components/talents/Edit/Education';
import PreferencesTalent, {form as preferencesFormProperties} from 'components/talents/Edit/Preferences';
import RequirementsTalent, {form as requirementsFormProperties} from 'components/talents/Edit/Requirements';
import PitchTalent, { form as pitchFormProperties} from 'components/talents/Edit/Pitch';
import type { $stateProps as $getTalentProps} from '../get'
import NextButton from 'components/talents/Edit/NextButton';
import Yup from 'yup';

type $stateProps = {}
type $ownProps = {}
type $dispatchProps = {
  updateTalent: Function,
};
type $formProps = {
  generalForm: Object,
  pitchForm: Object,
  jobForm: Object,
  preferencesForm: Object,
  workForm: Object,
  educationForm: Object,
  linksForm: Object,
  requirementsForm: Object,
  settingsForm: Object,
}
type $props = $stateProps & $dispatchProps & $ownProps & $formProps & $getTalentProps;

const orderOfValidity = [
  'pitch', 'job', 'requirements', 'preferences', 'education', 'work', 'links', 'settings']

const state = {
  index: 0,
};
export class TabsTalent extends React.Component<$props, typeof state> {
  state = state;
  isDisabled = (name: string) => {
    const index = orderOfValidity.indexOf(name);
    let i = 0;
    let disabled = false;
    while(i < index && !disabled){
      const previousName = orderOfValidity[i];
      if (!this.props[`${previousName}Form`].isValid) {
        disabled = true;
      }
      i++;
    }
    return disabled;
  }
  render(){
    const { pitchForm, jobForm, preferencesForm, workForm, educationForm, linksForm, requirementsForm, settingsForm, ...props } = this.props
    return <Tabs onTabChange={this.props.setIndex} index={this.props.index}>
      <Tab label="Pitch" disabled={this.isDisabled('pitch')}>
        <PitchTalent 
          {...{
            ...props,
            ...pitchForm,
          }}
        />
        <NextButton {...pitchForm} setIndex={this.props.setIndex.bind(this, 1)} />
      </Tab>
      <Tab label="Job" disabled={this.isDisabled('job')}>
        <JobTalent 
          {...{
            ...props,
            ...jobForm,
          }}
        />
        <NextButton {...jobForm} setIndex={this.props.setIndex.bind(this, 2)} />
      </Tab>
      <Tab label="Requirements" disabled={this.isDisabled('requirements')}>
        <RequirementsTalent {...{
          ...props,
          ...requirementsForm,
        }} />
        <NextButton {...requirementsForm} setIndex={this.props.setIndex.bind(this, 3)} />
      </Tab>
      <Tab label="Preferences" disabled={this.isDisabled('preferences')}>
        <PreferencesTalent {...{
          ...props,
          ...preferencesForm,
        }} />
        <NextButton {...preferencesForm} setIndex={this.props.setIndex.bind(this, 4)} />
      </Tab>
      <Tab label="Education" disabled={this.isDisabled('education')}>
        <EducationTalent {...{
          ...props,
          ...educationForm,
        }} />
        <NextButton {...educationForm} setIndex={this.props.setIndex.bind(this, 5)} />
      </Tab>  
      <Tab label="Work" disabled={this.isDisabled('work')}>
        <WorkTalent
        {...{
          setIndex: this.props.setIndex.bind(this, 6),
          ...props,
          ...workForm,
        }}
         />
         <NextButton {...workForm} setIndex={this.props.setIndex.bind(this, 7)} />
      </Tab>
      <Tab label="Links" disabled={this.isDisabled('links')}>
        <LinksTalent {...{
          ...props,
          ...linksForm,
        }} />
        
      </Tab>
      <Tab label="Settings" disabled={this.isDisabled('settings')}>
        <SettingsTalent {...{
          ...props,
          ...settingsForm,
        }} />
        
      </Tab>
    </Tabs>
  }
}

const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  
})

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps & $getTalentProps) =>
  bindActionCreators(
    {
      updateTalent: values => updateTalent(id, values),
    },
    dispatch
  );
  

const addInitialValidCheck = (opt)=>{
  return {
    ...opt,
    isInitialValid: (props)=>{
      const validationSchema = opt.validationSchema instanceof Yup.object ? opt.validationSchema : opt.validationSchema(props);
      // try{
      //   console.log('validating', opt.array);
      //   validationSchema.validateSync(opt.mapPropsToValues(props));
      // } catch (e) {
      //   console.log(e);
      // }
      return validationSchema.isValidSync(opt.mapPropsToValues(props));
    }
  }
}

export default flowRight([
  getTalent,
  connect(mapStateToProps, mapDispatchToProps),
  form({ ...addInitialValidCheck(pitchFormProperties), propName: 'pitchForm'}),
  form({ ...addInitialValidCheck(jobFormProperties), propName: 'jobForm'}),
  form({ ...addInitialValidCheck(preferencesFormProperties), propName: 'preferencesForm'}),
  form({ ...addInitialValidCheck(workFormProperties), propName: 'workForm'}),
  form({ ...addInitialValidCheck(educationFormProperties), propName: 'educationForm'}),
  form({ ...addInitialValidCheck(linksFormProperties), propName: 'linksForm'}),
  form({ ...addInitialValidCheck(requirementsFormProperties), propName: 'requirementsForm'}),
  form({ ...addInitialValidCheck(settingsFormProperties), propName: 'settingsForm'}),
])(TabsTalent)
