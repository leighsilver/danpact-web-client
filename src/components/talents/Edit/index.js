// @flow
import * as React from 'react'
import TalentTabs from './Tabs';
const state = {
  index: 5,
};
export default class EditTalent extends React.Component<Object, typeof state> {
  state = state;
  setIndex = (index: number) => {
    this.setState({
      index
    })
  }
  render(){
    return <TalentTabs index={this.state.index} setIndex={this.setIndex} />
  }
}