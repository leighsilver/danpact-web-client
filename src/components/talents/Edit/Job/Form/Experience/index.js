// @flow
import * as React from 'react'
import { jobSubtypesById } from '@client/utils/jobs';
import Experience from 'components/general/form/Experience';

type $props = Object

export default class JobExperience extends React.PureComponent<$props> {
  onChange = (id, value) => {
    this.props.onChange({
      ...this.props.value,
      [id]: value
    });
  }
  render(){
    const { ...props } = this.props
    return  <React.Fragment>
      <h3 style={{textAlign: 'left'}}>Experience</h3>
      {
        (props.jobSubtypes).map((id)=>{
          return <div key={id}>
            <span>{jobSubtypesById[id]}<Experience value={this.props.value[id] || null} onChange={this.onChange.bind(this, id)} /></span>
          </div>
        })
      }
    </React.Fragment>
  }
}