// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { ULItem, UL } from 'ui-kit';
import JobSubtype from 'components/general/form/JobSubtype';
import JobType from 'components/general/form/JobType';
import { jobTypes } from '@client/utils/jobs';
import Experience from './Experience';
import Skills from 'components/general/form/Skills';
type $props = {
  fields: Object;
};

type $dispatchProps = {
  searchSkills: Function
}

export class JobForm extends React.PureComponent<$props & $dispatchProps> {
  renderSubtypes = (values: Array<$$id>) => {
    return <UL>{values.map((id, i) => <ULItem key={i} />)}</UL>;
  };
  render() {
    const { props } = this;
    return (
      <React.Fragment>
      <JobType {...props.fields.jobType} disabled value={jobTypes.developer.id} />
        <JobSubtype {...props.fields.jobSubtypes} />
        <Experience jobSubtypes={props.fields.jobSubtypes.value} {...props.fields.experience} />
        <Skills {...props.fields.workSkills} />
        <Skills {...props.fields.sideProjectSkills} />
      </React.Fragment>
    );
  }
}

export default flowRight([
])(JobForm);
