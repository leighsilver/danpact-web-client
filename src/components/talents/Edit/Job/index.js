// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { jobValidationSchema as validationSchema } from '@client/utils/talents';
import JobForm from './Form';
import type { $stateProps as $getTalentProps} from '../../get'

type $stateProps = {};
type $dispatchProps = {
};
type $ownProps = $getTalentProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class GeneralTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <JobForm fields={props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) =>
  bindActionCreators(
    {
    },
    dispatch
  );


export const form = {
  mapPropsToValues: ({ talent }) => {
    return ({
      jobType: talent.jobType,
      jobSubtypes: talent.jobSubtypes.toArray(),
      experience: talent.experience.toJS(),
      workSkills: talent.workSkills.toArray(),
      sideProjectSkills: talent.sideProjectSkills.toArray(),
    })
  },
  validationSchema,
  handleChange: props => {
    return (key, onChange) => {
      return value => {
        onChange(value);
        return props.updateTalent({ [key]: value });
      };
    };
  },
};

export default flowRight([
  connect(null, mapDispatchToProps),
])(GeneralTalent);
