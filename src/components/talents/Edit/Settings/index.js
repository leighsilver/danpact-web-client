// @flow

import React from 'react';
import { flowRight } from 'lodash';

import { validationSchema } from '@client/utils/talents';
import GeneralForm from './Form';
import type { $stateProps as $getTalentProps} from '../../get'

type $stateProps = {};

type $ownProps = $getTalentProps;

type $props = $stateProps & $ownProps & $$formProps;

export class SettingsTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return <GeneralForm fields={props.fields} />;
  }
}

export const form = {
  mapPropsToValues: ({ talent }) => ({
    active: talent.active,
    blockCompanies: talent.blockCompanies.toJS(),
  }),
  validationSchema,
  handleChange: props => {
    return (key, onChange) => {
      return value => {
        onChange(value);
        return props.updateTalent({ [key]: value });
      };
    };
  },
};

export default flowRight([
])(SettingsTalent);
