// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput, Checkbox } from 'ui-kit';

import SelectDropdown from 'ui-kit/SelectDropdown';
import Switch from 'ui-kit/Switch';
import BlockCompanies from 'components/talents/Edit/Settings/Form/BlockCompanies';

type $props = {
  fields: Object;
};

const profileVisibilityOptions = [
  {
    id: 1, name: 'Visible to all'
  },
  {
    id: 1, name: 'Invisible to all'
  },
]

export class SettingsForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>        
        <BlockCompanies {...props.fields.blockCompanies} />
        <Switch {...props.fields.active} label="Activate Profile" />
        {
          props.fields.active.value && <p>Congratulations, your profile is now visible to employers. You will be notified by email when companies request to interview you. Please remember to look over the details of any interview requests and make sure that they match, at the very least, your mimimum expectations.</p>
        }
        
      </React.Fragment>
    );
  }
}

export default flowRight()(SettingsForm);
