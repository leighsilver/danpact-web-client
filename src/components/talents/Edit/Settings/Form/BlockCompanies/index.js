// @flow
import * as React from 'react'
import { TextInput } from 'ui-kit/TextInput';
import Button from 'ui-kit/Button';
import UL from 'ui-kit/UL';
import ULItem from 'ui-kit/UL/Item';
import uuid from 'uuid/v1';
import Clickable from 'ui-kit/Clickable';

type $props = Object
const state = {
  name: '',
  id: uuid(),
}
export default class BlockCompanies extends React.PureComponent<$props, typeof state> {
  state = state;
  onChange = (name: string) => {
    this.setState({name})
  }
  onBlock = () => {
    this.props.onChange((this.props.value || []).concat([
      this.state,
    ]))
    this.setState({
      name: '',
      id: uuid(),
    })
  }
  unBlock = (id: $$id) => {
    this.props.onChange(this.props.value.filter(c => c.id !==id))
  }
  render(){
    const { ...props } = this.props
    return <React.Fragment>
      <span><TextInput key={this.state.id} label="Create a list company names to block from seeing your profile" value={this.state.name} onChange={this.onChange}/><Button disabled={!this.state.name} onClick={this.onBlock}>Block</Button></span>    
      <h4>Blocked Companies</h4>
      <UL>
        {
          props.value.map((company)=>{
            return <ULItem key={company.id} caption={company.name} rightActions={[<Clickable key={company.id} onClick={this.unBlock.bind(this, company.id)}>X</Clickable>]} />
          })
        }
      </UL>
      
      
    </React.Fragment>
  }
}