// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput, Checkbox } from 'ui-kit';

type $props = {
  fields: Object;
};

export class GeneralForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <TextInput {...props.fields.name} />
        <TextInput {...props.fields.phone} />
        <TextInput {...props.fields.city} />
        <Checkbox {...props.fields.remote} />
        <Checkbox {...props.fields.relocate} />
      </React.Fragment>
    );
  }
}

export default flowRight()(GeneralForm);
