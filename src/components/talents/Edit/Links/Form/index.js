// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput } from 'ui-kit';

type $props = {
  fields: Object;
};

export class LinksForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <TextInput debounce {...props.fields.personalWebsite} />
        <TextInput debounce {...props.fields.blog} />
        <TextInput debounce {...props.fields.resume} />
        <TextInput debounce {...props.fields.linkedIn} />
        <TextInput debounce {...props.fields.github} />
        <TextInput debounce {...props.fields.facebook} />
        <TextInput debounce {...props.fields.twitter} />
        <TextInput debounce {...props.fields.stackOverflow} />
      </React.Fragment>
    );
  }
}

export default flowRight()(LinksForm);
