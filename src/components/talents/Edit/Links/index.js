// @flow

import React from 'react';
import { flowRight } from 'lodash';

import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { linksValidationSchema as validationSchema } from '@client/utils/talents';
import GeneralForm from './Form';
import type { $stateProps as $getTalentProps} from '../../get'

type $stateProps = {};
type $dispatchProps = {
};
type $ownProps = $getTalentProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class LinksTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <GeneralForm fields={props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {},
    dispatch
  );


export const form = {
  mapPropsToValues: ({ talent }) => ({
    personalWebsite: talent.personalWebsite,
    blog: talent.blog,
    github: talent.github,
    linkedIn: talent.linkedIn,
    facebook: talent.facebook,
    twitter: talent.twitter,
    stackOverflow: talent.stackOverflow,
    resume: talent.resume,
  }),
  validationSchema,
  handleChange: props => {
    return (key, onChange) => {
      return value => {
        onChange(value);
        return props.updateTalent({ [key]: value });
      };
    };
  },
};

export default flowRight([
  connect(null, mapDispatchToProps),
])(LinksTalent);
