// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput } from 'ui-kit';

type $props = {
  fields: Object;
};

export class GeneralForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <TextInput debounce multiline rows={5} {...props.fields.snippet} label="A snippet about yourself that catches the attention of hiring companies, like a strong office or open-source accomplishment" />
        <TextInput debounce multiline rows={5} {...props.fields.previousWork} />
        <TextInput debounce multiline rows={5} {...props.fields.nextWork} />
      </React.Fragment>
    );
  }
}

export default flowRight()(GeneralForm);
