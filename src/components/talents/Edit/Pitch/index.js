// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { pitchValidationSchema as validationSchema } from '@client/utils/talents';
import PitchForm from './Form';
import type { $stateProps as $getTalentProps} from '../../get'

type $stateProps = {};
type $dispatchProps = {};
type $ownProps = $getTalentProps;
type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class PitchTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <PitchForm fields={props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {
    },
    dispatch
  );


const mapPropsToValues = ({ talent }: $props) => {
  return ({
    snippet: talent.snippet,
    previousWork: talent.previousWork,
    nextWork: talent.nextWork,
  })
}
export const form = {
  isInitialValid: (props: $props)=>{
    return validationSchema.isValid(mapPropsToValues(props));
  },
  mapPropsToValues,
  validationSchema,
  handleChange: props => {
    return (key, onChange) => {
      return value => {
        onChange(value);
        return props.updateTalent({ [key]: value });
      };
    };
  },
}

export default flowRight([
  connect(null, mapDispatchToProps),
])(PitchTalent);
