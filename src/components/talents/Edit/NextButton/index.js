// @flow
import * as React from 'react';
import Button from 'ui-kit/Button';
type $props = {
  setIndex: Function,
  isValid: boolean,
};

export default class NextButton extends React.PureComponent<$props> {
  render(){
    return <Button onClick={this.props.setIndex} disabled={!this.props.isValid}>Next</Button>
  }
}