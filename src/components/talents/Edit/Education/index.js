// @flow

import React from 'react';
import { flowRight } from 'lodash';
import EducationForm from './Form';
import type { $stateProps as $getTalentProps} from '../../get'
import Yup from 'yup';
import { rawSchoolValidationSchema } from '@client/utils/talents';
import { getArrayValidationSchema, getMapPropsToValues } from '@client/hocs/form';

type $stateProps = {};
type $dispatchProps = {
};
type $ownProps = $getTalentProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps & $$formParentProps;

export class EducationTalent extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <EducationForm reinitializeForm={this.props.reinitializeForm} id={props.id} fields={props.fields.arrayFields.education} education={this.props.talent.education} updateTalent={props.updateTalent} />
    );
  }
}

const mapPropsToValues = ({talent}) => ({education: talent.education.toJS()})
const arrayPropsObject =   {
  education: rawSchoolValidationSchema
};

const validationSchema = getArrayValidationSchema(mapPropsToValues, arrayPropsObject, { education: Yup.array().min(1).required() });

export const form = {
  mapPropsToValues: getMapPropsToValues(mapPropsToValues, arrayPropsObject),
  validationSchema,
  array: arrayPropsObject,
  handleChange: props => {
    return (key, onChange, arrayProp, getNextArray) => {
      return value => {
        onChange(value);
        return props.updateTalent({ [arrayProp]: getNextArray(props.talent.education.toJS(), value) });
      };
    };
  },
};

export default flowRight([])(EducationTalent);
