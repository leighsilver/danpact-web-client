// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { Button } from 'ui-kit';
import School from './School';
import uuid from 'uuid/v1';
import { List } from 'immutable';
import UL from 'ui-kit/UL';
import ULItem from 'ui-kit/UL/Item';
import Clickable from 'ui-kit/Clickable';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { updateLocal } from '@client/actions/talents';
import * as talentServices from '@client/services/talents';

type $ownProps = {
  id: $$id;
  fields: Object;
  updateTalent: Function;
  education: List<Object>;
  reinitializeForm: Object;
};

type $dispatchProps = {
  updateTalentLocal: Function,
}

type $props = $ownProps & $dispatchProps;

export class EducationForm extends React.PureComponent<$props > {
  addSchool = () => {
    const education = this.props.education.toJS().concat([{id: uuid()}]);
    this.props.updateTalent({ education }).then(()=>{
      this.props.reinitializeForm.educationForm();
    })
  }
  deleteSchool = (index: number) => {
    const education = this.props.education.toJS()
    education.splice(index, 1);
    this.props.updateTalentLocal({ education })
    this.props.reinitializeForm.educationForm();
    talentServices.update(this.props.id, { education  });
  }
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <UL>
          {
            props.fields.map((fields, index) => {
              return <ULItem key={index} ripple={false} content={<School fields={fields} id={this.props.id} />} rightActions={[
                <Clickable key="deleteSchool" onClick={()=>this.deleteSchool(index)}>x</Clickable>
              ]}/>
            })
          }
        </UL>
        <Button onClick={this.addSchool}>+</Button>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps : $$selectorExact<$dispatchProps> = (dispatch, props)=>bindActionCreators({
  updateTalentLocal: (values)=>updateLocal(props.id, values),
}, dispatch)

export default flowRight([
  connect(null, mapDispatchToProps),
])(EducationForm);
