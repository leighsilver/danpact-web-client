// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput, Row, Col, Checkbox } from 'ui-kit';
import SelectDropdown from 'ui-kit/SelectDropdown';

type $props = {
  fields: Object;
};

const schoolTypeOptions = [
  {
    id: 1,
    name: 'PhD',
  },
  {
    id: 2,
    name: 'Masters',
  },
  {
    id: 3,
    name: 'Bachelors',
  },
  {
    id: 4,
    name: 'Diploma / Certificate',
  },
  {
    id: 5,
    name: 'Online',
  },
  {
    id: 6,
    name: 'High School',
  },
]

export class SchoolForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <Row>
        <Col xs={4}>
            <SelectDropdown sync options={schoolTypeOptions} {...props.fields.type} />
          </Col>
          <Col xs={4}>
            <TextInput debounce {...props.fields.name} />
          </Col>
          <Col xs={4}>
            <TextInput debounce {...props.fields.degree} />
          </Col>
        </Row>
        <Row>
          <Col xs={9}>
            <TextInput debounce rows={5} multiline {...props.fields.additionalComments} />
          </Col>
          <Col xs={3}>
            <TextInput debounce type="number" {...props.fields.graduationYear} />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default flowRight()(SchoolForm);
