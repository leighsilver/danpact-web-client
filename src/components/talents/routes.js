// @flow
import * as React from 'react';
import { Route } from 'react-router';
import ShowAll from './ShowAll';
import Show from './Show';
import Edit from './Edit';


export default [
  <Route exact key="ShowAll" path="/talents" component={ShowAll} />,
  <Route exact key="Edit" path="/talents/:talentId/edit" component={Edit} />,
  <Route exact key="Show" path="/positions/:positionId/talents/:talentId" component={Show} />,
]
