// @flow
import * as React from 'react';
import { ULItem } from 'ui-kit';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import Micon from 'ui-kit/Micon';
import User from '@client/models/User';
import * as talentSelectors from '@client/selectors/talents';
import * as userSelectors from '@client/selectors/users';
import { goToEditTalent, goToTalent } from '@client/actions/talents';
import { bindActionCreators } from '@client/utils/components';
import Talent from '@client/models/Talent'

type $stateProps = {
  talent: Talent,
  user: User
};

type $ownProps = {
  id: $$id,
  canEdit?: boolean,
};

type $dispatchProps = {
  goToEditTalent: Function,
  goToTalent: Function,
};

type $props = $ownProps & $stateProps & $dispatchProps;

export class TalentItem extends React.PureComponent<$props> {
  render() {
    const { canEdit, goToEditTalent, goToTalent, ...props } = this.props;
    return (
      <ULItem
        {...props}
        rightActions={[
          <p>x</p>
        ]}
        onClick={goToTalent}
        caption={<div>
          <p>{props.user.fullName}</p>
          <p>{`Location: ${props.user.location}`}</p>
          <p>{`Compensation: ${props.talent.compensation}`}</p>
        </div>}
      />
    );
  }
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  talent: talentSelectors.find(),
  user: userSelectors.find(userSelectors.findRelated('user')),
});

export const mapDispatchToProps = (dispatch: $$dispatch, props: $props) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToTalent: () => goToTalent(props.positionId, props.id)
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TalentItem);
