
// @flow
import * as React from 'react'
import { SelectDropdown } from 'ui-kit';
import { jobTypes } from '@client/utils/jobs';

const subtypeValues = Object.values(jobTypes.developer.subtypes)

type $props = Object
export default class JobSubtype extends React.PureComponent<$props> {
  render(){
    return <SelectDropdown
      multi
      sync
      options={subtypeValues}
      {...this.props}
    />
  }
}