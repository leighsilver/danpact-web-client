// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { connect } from 'react-redux';
import { SelectDropdown } from 'ui-kit';

import { bindActionCreators } from 'redux';
import * as skillActions from '@client/actions/skills';

type $props = {
  fields: Object;
};

type $dispatchProps = {
  searchSkills: Function
}

export class Skills extends React.PureComponent<$props & $dispatchProps> {
  search = (text) => {
    return this.props.searchSkills(text, this.props.value ? Array.isArray(this.props.value) ? this.props.value : [this.props.value] : [] );
  }
  render() {
    const { searchSkills, ...props } = this.props;
    return (<SelectDropdown
      key={props.value}
      multi
      loadOptions={this.search}
      {...props}
    />);
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({
  searchSkills: skillActions.search,
}, dispatch);

export default flowRight([
  connect(null, mapDispatchToProps)
])(Skills);
