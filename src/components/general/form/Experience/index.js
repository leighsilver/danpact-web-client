// @flow
import * as React from 'react'
import SelectDropdown from 'ui-kit/SelectDropdown';

type $props = {};

const options = [
  {
    id: '',
    name: 'Years Experience'
  },
  {
    id: '0-1',
    name: '0-1 years'
  },
  {
    id: '1-2',
    name: '1-2 years'
  },
  {
    id: '2-3',
    name: '2-3 years'
  },
  {
    id: '3-5',
    name: '3-5 years'
  },
  {
    id: '5-7',
    name: '5-7 years'
  },
  {
    id: '8+',
    name: '8+ years'
  },
]


export default class Experience extends React.PureComponent<$props> {
  render(){
    return <SelectDropdown sync options={options} {...this.props} />
  }
};
