// @flow
import * as React from 'react'
import { SelectDropdown } from 'ui-kit';

type $props = {
  
};

export const options = [
  {id: 0, name: 'No preference'},
  {id: 1, name: '0-10 employees'},
  {id: 2, name: '10-20 employees'},
  {id: 3, name: '20-50 employees'},
  {id: 4, name: '50-200 employees'},
  {id: 5, name: '200+ employees'},
]

export default class CompanySize extends React.PureComponent<$props> {
  render(){
    const { ...props } = this.props
    return <SelectDropdown sync options={options} {...props} />
  }
}
