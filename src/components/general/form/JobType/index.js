
// @flow
import * as React from 'react'
import { SelectDropdown } from 'ui-kit';
import { jobTypes } from '@client/utils/jobs';

const options = Object.values(jobTypes)

type $props = Object
export default class JobType extends React.PureComponent<$props> {
  render(){
    return <SelectDropdown
      multi
      sync
      options={options}
      {...this.props}
    />
  }
}