
// @flow
import * as React from 'react'
import { SelectDropdown } from 'ui-kit';

import fetch from 'isomorphic-fetch';

type $props = Object

export default class Locations extends React.PureComponent<$props> {
  render(){
    return <SelectDropdown
      key={this.props.value} 
      loadOptions={(search)=>{
        return fetch('http://localhost:8000', {
          method: 'POST',
          body: JSON.stringify({
            search,
            ids: this.props.value ? Array.isArray(this.props.value) ? this.props.value : [this.props.value] : []
          })
        }).then((resp)=>{
          return resp.json()
        }).then((resp)=>{
          return resp.map(l => ({
            ...l,
            name: `${l.city_name}, ${isNaN(l.region) ? `${l.region}, ` :  ''}${l.country_code.toUpperCase()}`
          }))
        })
      }}
      {...this.props}
    />
  }
}