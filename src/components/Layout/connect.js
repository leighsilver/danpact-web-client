// @flow

import { bindActionCreators } from '@client/utils/components';
import { checkIfLoggedIn } from '@client/actions/pages/sessions';
import { push } from '@client/actions/router';

export const mapDispatchToProps = (dispatch: $$dispatch) =>
  bindActionCreators(
    {
      checkIfLoggedIn,
      goHome: () => push('/'),
    },
    dispatch
  );
