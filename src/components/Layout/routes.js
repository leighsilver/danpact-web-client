//@flow
import React from 'react';
import { Switch, Route } from 'react-router';

import TestMy from 'components/tests/My';
import TestShowAll from 'components/tests/ShowAll';
import TestCreate from 'components/tests/Create';
import TestShow from 'components/tests/Show';
import TestEdit from 'components/tests/Edit';

import UsersCreate from 'components/users/Create';

import UsersShowEditContainer from 'components/users/ShowEditContainer';

import UsersShow from 'components/users/Show';
import UsersEdit from 'components/users/Edit';


import SessionsCreate from 'components/pages/sessions/Create';
import Home from 'components/pages/Home';

import discourseRoutes from 'components/discourses/routes';
import companyRoutes from 'components/companies/routes';
import positionRoutes from 'components/positions/routes';
import talentRoutes from 'components/talents/routes';

export default [
  <Switch key="switch">
    <Route exact path="/tests" component={TestShowAll} />
    <Route exact path="/tests/my" component={TestMy} />
    <Route exact path="/tests/create" component={TestCreate} />
    <Route exact path="/tests/:testId/edit" component={TestEdit} />
    <Route exact path="/tests/:testId" component={TestShow} />
    <Route exact path="/users/:userId/edit" component={UsersEdit} />
    <Route exact path="/users/:userId" component={UsersShow} />
    <Route exact path="/signup" component={UsersCreate} />
    <Route exact path="/login" component={SessionsCreate} />
    {
      talentRoutes
    }
    {
      discourseRoutes
    }
    {
      companyRoutes
    }
    {
      positionRoutes
    }
    <Route exact path="/" component={Home} />
  </Switch>
];
