// @flow
import * as React from 'react';
import { Route } from 'react-router';
import ShowAll from './ShowAll';
import Create from './Create';
import Show from './Show';
import Edit from './Edit';
import My from './My';

export default [
  <Route exact key="ShowAll" path="/positions" component={ShowAll} />,
  <Route exact key="Create" path="/positions/create" component={Create} />,
  <Route exact key="Edit" path="/positions/:positionId/edit" component={Edit} />,
  <Route exact key="My" path="/positions/my" component={My} />,
  <Route exact key="Show" path="/positions/:positionId" component={Show} />,
]
