// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { update as updatePosition } from '@client/actions/positions';
import { validationSchema } from '@client/utils/positions';
import PositionForm from '../Form';
import getPosition from '../get';
import type { $stateProps as $getPositionProps} from '../get'

type $stateProps = {};
type $dispatchProps = {
  updatePosition: Function,
};
type $ownProps = $getPositionProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class EditPosition extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>{props.position.name}</h1>
        <PositionForm fields={this.props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {
      updatePosition: values => updatePosition(id, values),
    },
    dispatch
  );

export default flowRight([
  getPosition,
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: ({ position }) => ({
      ...position.toObject(),
    }),
    validationSchema,
    handleChange: props => {
      return (key, onChange) => {
        return value => {
          onChange(value);
          return props.updatePosition({ [key]: value });
        };
      };
    },
  }),
])(EditPosition);
