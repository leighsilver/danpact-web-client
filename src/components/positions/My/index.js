// @flow
import React from 'react';
import { UL, ULItem } from 'ui-kit';
import { flowRight } from 'lodash';
import { push } from '@client/actions/router';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getPositions } from '@client/actions/pages/homes';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/pages/homes';
import PositionItem from '../Item';

type $stateProps = {
  positionIds: List<$$id>
};
type $dispatchProps = {
  goToCreatePosition: Function,
  getPositions: Function,
};
type $ownProps = {
  canEdit: boolean,
  userId: $$id,
};

type $props = $stateProps & $dispatchProps & $ownProps;

export class MyPositions extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getPositions();
  }
  render() {
    const { canEdit } = this.props;
    return (
      <div>
        <UL>
          {canEdit && (
            <ULItem
              onClick={this.props.goToCreatePosition}
              selectable
              caption="Create Position"
              leftIcon="add"
            />
          )}
          {this.props.positionIds.map(id => {
            return <PositionItem canEdit={canEdit} key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, props: $ownProps) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToCreatePosition: () => push('/positions/create'),
      getPositions: ()=>getPositions(props.userId)
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  positionIds: getRelated('positions')
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  MyPositions
);
