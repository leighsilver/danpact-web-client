// @flow

import khange, { kheck } from 'khange';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getParam } from '@client/selectors/router';
import { flowRight } from 'lodash';
import * as positionSelectors from '@client/selectors/positions';
import Position from '@client/models/Position'
import { get } from '@client/actions/positions';
import { bindActionCreators } from '@client/utils/components';
import { formParent } from '@client/hocs';

const getPositionId = getParam('positionId');

export type $stateProps = {
  id: $$id,
  position: Position,
  talentIds: List<$$id>,
};

type $dispatchProps = {
  get: Function,
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  id: getPositionId,
  position: positionSelectors.find(getPositionId),
  talentIds: positionSelectors.getRelated('talents', getPositionId),
});

export const mapDispatchToProps = (dispatch: $$dispatch): $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      get
    },
    dispatch
  );

  export const onKhange = (props: $$formParentProps & $stateProps & $dispatchProps) => {
  props.get(props.id).then(() => {
    if (props.reinitializeForm) props.reinitializeForm.go();
  });
};

export default flowRight([
  formParent,
  connect(mapStateToProps, mapDispatchToProps),
  khange(kheck('id'), onKhange)
])