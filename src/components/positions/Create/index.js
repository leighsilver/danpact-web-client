// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';

import { Button } from 'ui-kit';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { validationSchema } from '@client/utils/positions';
import { create as createPosition, goToPosition } from '@client/actions/positions';
import uuid from 'uuid/v1';
import PositionForm from '../Form';

type $ownProps = {
  
}

type $stateProps = {

}

type $dispatchProps = {
  createPosition: Function,
  goToPosition: Function,
}

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class CreatePosition extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>Create Position</h1>
        <PositionForm fields={this.props.fields} />
        <Button
          primary
          disabled={!props.isValid}
          onClick={this.props.handleSubmit}
        >
          Create
        </Button>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      createPosition,
      goToPosition
    },
    dispatch
  );

export default flowRight([
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: () => ({
      type: 0,
      subtype: 0,
      title: '',
      experience: 0,
      location: 'Toronto, ON',
      remote: false,
      relocate: false,
      sponsorship: false,
    }),
    validationSchema,
    handleSubmit: (values, { props }) => {
      const id = uuid();
      props.goToPosition(id);
      return props.createPosition({ ...values, id });
    }
  })
])(CreatePosition);
