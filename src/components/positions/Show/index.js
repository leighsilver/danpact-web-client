// @flow
import React from 'react';
import { flowRight } from 'lodash';
import { UL } from 'ui-kit';
import getPosition from '../get';
import PositionBreakdown from '../Breakdown';
import type { $stateProps } from '../get';
import TalentItem from 'components/talents/Item';

type $props = $stateProps;

export class ShowPosition extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <PositionBreakdown id={props.id} />
        <UL>
          {
            props.talentIds.map(id => <TalentItem key={id} positionId={props.id} id={id} />)
          }
        </UL>
      </div>
    );
  }
}

export default flowRight([getPosition])(ShowPosition);
