// @flow
import * as React from 'react';
import { ULItem } from 'ui-kit';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import * as positionSelectors from '@client/selectors/positions';
import { goToEditPosition, goToPosition } from '@client/actions/positions';
import { bindActionCreators } from '@client/utils/components';
import Position from '@client/models/Position'

type $stateProps = {
  position: Position
};

type $ownProps = {
  id: $$id,
  canEdit?: boolean,
};

type $dispatchProps = {
  goToEditPosition: Function,
  goToPosition: Function,
};

type $props = $ownProps & $stateProps & $dispatchProps;

export class PositionItem extends React.PureComponent<$props> {
  render() {
    const { canEdit, position, goToEditPosition, goToPosition, ...props } = this.props;
    return (
      <ULItem
        {...props}
        onClick={canEdit ? goToEditPosition : goToPosition}
        caption={position.title}
      />
    );
  }
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  position: positionSelectors.find()
});

export const mapDispatchToProps = (dispatch: $$dispatch, props: $props) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToEditPosition: () => goToEditPosition(props.id),
      goToPosition: () => goToPosition(props.id)
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PositionItem);
