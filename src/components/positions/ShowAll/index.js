// @flow
import React from 'react';
import { UL } from 'ui-kit';
import { flowRight } from 'lodash';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getPositions } from '@client/actions/pages/homes';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/pages/homes';
import PositionItem from '../Item';

type $stateProps = {
  positionIds: List<$$id>
};
type $dispatchProps = {
  getPositions: Function,
};
type $ownProps = {

};

type $props = $stateProps & $dispatchProps & $ownProps;

export class ShowAllPositions extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getPositions();
  }
  render() {
    return (
      <div>
        <UL>
          {this.props.positionIds.map(id => {
            return <PositionItem key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      getPositions
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  positionIds: getRelated('positions')
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  ShowAllPositions
);
