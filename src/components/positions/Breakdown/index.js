// @flow
import * as React from 'react'
import { flowRight, bindActionCreators } from '@client/utils/components'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import Position from '@client/models/Position';
import { find } from '@client/selectors/positions';

type $stateProps = {position: Position}
type $ownProps = {}
type $dispatchProps = {}
type $props = $stateProps & $dispatchProps & $ownProps
export class  PositionBreakdown extends React.PureComponent<$props> {
  render(){
    const { ...props } = this.props
    return  <div>
      <h3>{props.position.title}</h3>
    </div>
  }
}

const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  position: find()
});

const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> => bindActionCreators({
}, dispatch)

export default flowRight([
  connect(mapStateToProps, mapDispatchToProps)
])( PositionBreakdown)