// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput } from 'ui-kit';
import JobType from 'components/general/form/JobType';
import JobSubtype from 'components/general/form/JobSubtype';
import Experience from 'components/general/form/Experience';
import Locations from 'components/general/form/Locations';
import { Checkbox } from 'ui-kit';

type $props = {
  fields: Object;
};

export class PositionForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <JobType {...props.fields.type} />
        <JobSubtype {...props.fields.subtype} />
        <TextInput {...props.fields.title} />
        <Experience {...props.fields.experience} />
        <Locations {...props.fields.location} />
        <Checkbox {...props.fields.remote} />
        <Checkbox {...props.fields.relocate} />
        <Checkbox {...props.fields.sponsorship} />
      </React.Fragment>
    );
  }
}

export default flowRight()(PositionForm);
