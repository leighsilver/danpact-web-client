// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput } from 'ui-kit';

type $props = {
  fields: Object;
};

export class CommentForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <TextInput rows={5} multiline {...props.fields.content} label="Add Comment"/>
    );
  }
}

export default flowRight()(CommentForm);
