// @flow

import khange, { kheck } from 'khange';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getParam } from '@client/selectors/router';
import { flowRight } from 'lodash';
import * as commentSelectors from '@client/selectors/comments';
import Comment from '@client/models/Comment'
import { get } from '@client/actions/comments';
import { bindActionCreators } from '@client/utils/components';
import { formParent } from '@client/hocs';

const getCommentId = getParam('commentId');

export type $stateProps = {
  id: $$id,
  comment: Comment,
};

type $dispatchProps = {
  get: Function,
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  id: getCommentId,
  comment: commentSelectors.find(getCommentId)
});

export const mapDispatchToProps = (dispatch: $$dispatch): $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      get
    },
    dispatch
  );

  export const onKhange = (props: $$formParentProps & $stateProps & $dispatchProps) => {
  props.get(props.id).then(() => {
    if (props.reinitializeForm) props.reinitializeForm.go();
  });
};

export default flowRight([
  formParent,
  connect(mapStateToProps, mapDispatchToProps),
  khange(kheck('id'), onKhange)
])