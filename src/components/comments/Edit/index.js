// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { update as updateComment } from '@client/actions/comments';
import { validationSchema } from '@client/utils/comments';
import CommentForm from '../Form';
import getComment from '../get';
import type { $stateProps as $getCommentProps} from '../get'

type $stateProps = {};
type $dispatchProps = {
  updateComment: Function,
};
type $ownProps = $getCommentProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class EditComment extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>{props.comment.name}</h1>
        <CommentForm fields={this.props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {
      updateComment: values => updateComment(id, values),
    },
    dispatch
  );

export default flowRight([
  getComment,
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: ({ comment }) => ({
      name: comment.name,
      purpose: comment.purpose,
      notes: comment.notes,
      instructives: comment.instructives,
      scope: comment.scope,
      refs: comment.refs
    }),
    validationSchema,
    handleChange: props => {
      return (key, onChange) => {
        return value => {
          onChange(value);
          return props.updateComment({ [key]: value });
        };
      };
    },
  }),
])(EditComment);
