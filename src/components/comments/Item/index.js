// @flow
import * as React from 'react';
import { ULItem } from 'ui-kit';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import * as commentSelectors from '@client/selectors/comments';
import { goToEditComment, goToComment } from '@client/actions/comments';
import { bindActionCreators } from '@client/utils/components';
import Comment from '@client/models/Comment'

type $stateProps = {
  comment: Comment
};

type $ownProps = {
  id: $$id,
  canEdit?: boolean,
};

type $dispatchProps = {
  goToEditComment: Function,
  goToComment: Function,
};

type $props = $ownProps & $stateProps & $dispatchProps;

export class CommentItem extends React.PureComponent<$props> {
  render() {
    const { canEdit, comment, goToEditComment, goToComment, ...props } = this.props;
    return (
      <ULItem
        {...props}
        onClick={canEdit ? goToEditComment : goToComment}
        caption={comment.content}
      />
    );
  }
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  comment: commentSelectors.find()
});

export const mapDispatchToProps = (dispatch: $$dispatch, props: $props) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToEditComment: () => goToEditComment(props.id),
      goToComment: () => goToComment(props.id)
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CommentItem);
