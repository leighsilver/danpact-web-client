// @flow
import React from 'react';
import { UL } from 'ui-kit';
import { flowRight } from 'lodash';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getComments } from '@client/actions/pages/homes';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/pages/homes';
import CommentItem from '../Item';

type $stateProps = {
  commentIds: List<$$id>
};
type $dispatchProps = {
  getComments: Function,
};
type $ownProps = {

};

type $props = $stateProps & $dispatchProps & $ownProps;

export class ShowAllComments extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getComments();
  }
  render() {
    return (
      <div>
        <UL>
          {this.props.commentIds.map(id => {
            return <CommentItem key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      getComments
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  commentIds: getRelated('comments')
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  ShowAllComments
);
