// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';

import { Button } from 'ui-kit';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { validationSchema } from '@client/utils/comments';
import { create as createComment, goToComment } from '@client/actions/comments';
import uuid from 'uuid/v1';
import CommentForm from '../Form';

type $ownProps = {
  
}

type $stateProps = {

}

type $dispatchProps = {
  createComment: Function,
  goToComment: Function,
}

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class CreateComment extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <CommentForm fields={this.props.fields} />
        <Button
          primary
          disabled={!props.isValid}
          onClick={this.props.handleSubmit}
        >
          Create
        </Button>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      createComment,
      goToComment
    },
    dispatch
  );

export default flowRight([
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: () => ({
      content: '',
    }),
    validationSchema,
    handleSubmit: (values, { props }) => {
      const id = uuid();
      return props.createComment({ ...values, id });
    }
  })
])(CreateComment);
