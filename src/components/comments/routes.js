// @flow
import * as React from 'react';
import { Route } from 'react-router';
import ShowAll from './ShowAll';
import Create from './Create';
import Show from './Show';
import Edit from './Edit';
import IssuesShowAll from 'components/issues/ShowAll/Comments'

export default [
  <Route exact key="ShowAll" path="/comments" component={ShowAll} />,
  <Route exact key="Create" path="/comments/create" component={Create} />,
  <Route exact key="Edit" path="/comments/:commentId/edit" component={Edit} />,
  <Route exact key="IssuesShowAll" path="/comments/:commentId/issues" component={IssuesShowAll} />,
  <Route exact key="Show" path="/comments/:commentId" component={Show} />,
]
