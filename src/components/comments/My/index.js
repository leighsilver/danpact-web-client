// @flow
import React from 'react';
import { UL, ULItem } from 'ui-kit';
import { flowRight } from 'lodash';
import { push } from '@client/actions/router';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getComments } from '@client/actions/users';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/users';
import CommentItem from '../Item';

type $stateProps = {
  commentIds: List<$$id>
};
type $dispatchProps = {
  goToCreateComment: Function,
  getComments: Function,
};
type $ownProps = {
  canEdit: boolean,
  userId: $$id,
};

type $props = $stateProps & $dispatchProps & $ownProps;

export class MyComments extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getComments();
  }
  render() {
    const { canEdit } = this.props;
    return (
      <div>
        <UL>
          {canEdit && (
            <ULItem
              onClick={this.props.goToCreateComment}
              selectable
              caption="Create Comment"
              leftIcon="add"
            />
          )}
          {this.props.commentIds.map(id => {
            return <CommentItem canEdit={canEdit} key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, props: $ownProps) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToCreateComment: () => push('/comments/create'),
      getComments: ()=>getComments(props.userId)
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  commentIds: getRelated('comments', (state, props)=>props.userId)
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  MyComments
);
