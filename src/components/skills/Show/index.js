// @flow
import React from 'react';
import { flowRight } from 'lodash';
import ULItem from 'ui-kit/UL/Item';
import UL from 'ui-kit/UL';
import getSkill from '../get';
import type { $stateProps } from '../get';

type $props = $stateProps;

class ShowText extends React.PureComponent<Object> {
  render() {
    const { props } = this;
    return (
      <div>
        <h3>{props.title}</h3>
        <p>{props.value}</p>
      </div>
    );
  }
}

class ShowMulti extends React.PureComponent<Object> {
  render() {
    const { props } = this;
    return (
      <div>
        <h3>{props.title}</h3>
        <UL>
          {props.values.map((value, i) => {
            return <ULItem key={i} caption={value} />;
          })}
        </UL>
      </div>
    );
  }
}

export class ShowSkill extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>{props.skill.name}</h1>
        <ShowText title="Notes" value={props.skill.notes} />
        <ShowText
          title="Instructives / Amounts"
          value={props.skill.instructives}
        />
        <ShowText title="Scope" value={props.skill.scope} />
        <ShowMulti title="References" values={props.skill.refs} />
      </div>
    );
  }
}

export default flowRight([getSkill])(ShowSkill);
