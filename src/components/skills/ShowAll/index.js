// @flow
import React from 'react';
import { UL } from 'ui-kit';
import { flowRight } from 'lodash';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getSkills } from '@client/actions/pages/homes';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/pages/homes';
import SkillItem from '../Item';

type $stateProps = {
  skillIds: List<$$id>
};
type $dispatchProps = {
  getSkills: Function,
};
type $ownProps = {

};

type $props = $stateProps & $dispatchProps & $ownProps;

export class ShowAllSkills extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getSkills();
  }
  render() {
    return (
      <div>
        <UL>
          {this.props.skillIds.map(id => {
            return <SkillItem key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      getSkills
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  skillIds: getRelated('skills')
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  ShowAllSkills
);
