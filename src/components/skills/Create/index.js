// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';

import { Button } from 'ui-kit';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { validationSchema } from '@client/utils/skills';
import { create as createSkill, goToSkill } from '@client/actions/skills';
import uuid from 'uuid/v1';
import SkillForm from '../Form';

type $ownProps = {
  
}

type $stateProps = {

}

type $dispatchProps = {
  createSkill: Function,
  goToSkill: Function,
}

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class CreateSkill extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>Create Skill</h1>
        <SkillForm fields={this.props.fields} />
        <Button
          primary
          disabled={!props.isValid}
          onClick={this.props.handleSubmit}
        >
          Create
        </Button>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      createSkill,
      goToSkill
    },
    dispatch
  );

export default flowRight([
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: () => ({
      name: '',
      notes: '',
      instructives: '',
      refs: []
    }),
    validationSchema,
    handleSubmit: (values, { props }) => {
      const id = uuid();
      props.goToSkill(id);
      return props.createSkill({ ...values, id });
    }
  })
])(CreateSkill);
