// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { update as updateSkill } from '@client/actions/skills';
import { validationSchema } from '@client/utils/skills';
import SkillForm from '../Form';
import getSkill from '../get';
import type { $stateProps as $getSkillProps} from '../get'

type $stateProps = {};
type $dispatchProps = {
  updateSkill: Function,
};
type $ownProps = $getSkillProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class EditSkill extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>{props.skill.name}</h1>
        <SkillForm fields={this.props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {
      updateSkill: values => updateSkill(id, values),
    },
    dispatch
  );

export default flowRight([
  getSkill,
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: ({ skill }) => ({
      name: skill.name,
      purpose: skill.purpose,
      notes: skill.notes,
      instructives: skill.instructives,
      scope: skill.scope,
      refs: skill.refs
    }),
    validationSchema,
    handleChange: props => {
      return (key, onChange) => {
        return value => {
          onChange(value);
          return props.updateSkill({ [key]: value });
        };
      };
    },
  }),
])(EditSkill);
