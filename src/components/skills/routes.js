// @flow
import * as React from 'react';
import { Route } from 'react-router';
import ShowAll from './ShowAll';
import Create from './Create';
import Show from './Show';
import Edit from './Edit';

export default [
  <Route exact key="ShowAll" path="/skills" component={ShowAll} />,
  <Route exact key="Create" path="/skills/create" component={Create} />,
  <Route exact key="Edit" path="/skills/:skillId/edit" component={Edit} />,
  <Route exact key="Show" path="/skills/:skillId" component={Show} />,
]
