// @flow
import React from 'react';
import { UL, ULItem } from 'ui-kit';
import { flowRight } from 'lodash';
import { push } from '@client/actions/router';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getSkills } from '@client/actions/users';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/users';
import SkillItem from '../Item';

type $stateProps = {
  skillIds: List<$$id>
};
type $dispatchProps = {
  goToCreateSkill: Function,
  getSkills: Function,
};
type $ownProps = {
  canEdit: boolean,
  userId: $$id,
};

type $props = $stateProps & $dispatchProps & $ownProps;

export class MySkills extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getSkills();
  }
  render() {
    const { canEdit } = this.props;
    return (
      <div>
        <UL>
          {canEdit && (
            <ULItem
              onClick={this.props.goToCreateSkill}
              selectable
              caption="Create Skill"
              leftIcon="add"
            />
          )}
          {this.props.skillIds.map(id => {
            return <SkillItem canEdit={canEdit} key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, props: $ownProps) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToCreateSkill: () => push('/skills/create'),
      getSkills: ()=>getSkills(props.userId)
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  skillIds: getRelated('skills', (state, props)=>props.userId)
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  MySkills
);
