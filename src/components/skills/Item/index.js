// @flow
import * as React from 'react';
import { ULItem } from 'ui-kit';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import * as skillSelectors from '@client/selectors/skills';
import { goToEditSkill, goToSkill } from '@client/actions/skills';
import { bindActionCreators } from '@client/utils/components';
import Skill from '@client/models/Skill'

type $stateProps = {
  skill: Skill
};

type $ownProps = {
  id: $$id,
  canEdit?: boolean,
};

type $dispatchProps = {
  goToEditSkill: Function,
  goToSkill: Function,
};

type $props = $ownProps & $stateProps & $dispatchProps;

export class SkillItem extends React.PureComponent<$props> {
  render() {
    const { canEdit, skill, goToEditSkill, goToSkill, ...props } = this.props;
    return (
      <ULItem
        {...props}
        onClick={canEdit ? goToEditSkill : goToSkill}
        caption={skill.name}
      />
    );
  }
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  skill: skillSelectors.find()
});

export const mapDispatchToProps = (dispatch: $$dispatch, props: $props) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToEditSkill: () => goToEditSkill(props.id),
      goToSkill: () => goToSkill(props.id)
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SkillItem);
