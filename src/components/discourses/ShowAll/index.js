// @flow
import React from 'react';
import { UL } from 'ui-kit';
import { flowRight } from 'lodash';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getDiscourses } from '@client/actions/pages/homes';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector } from 'reselect';
import { getRelated } from '@client/selectors/pages/homes';
import DiscourseItem from '../Item';

type $stateProps = {
  discourseIds: List<$$id>
};
type $dispatchProps = {
  getDiscourses: Function,
};
type $ownProps = {

};

type $props = $stateProps & $dispatchProps & $ownProps;

export class ShowAllDiscourses extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getDiscourses();
  }
  render() {
    return (
      <div>
        <UL>
          {this.props.discourseIds.map(id => {
            return <DiscourseItem key={id} id={id} />;
          })}
        </UL>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      getDiscourses
    },
    dispatch
  );

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  discourseIds: getRelated('discourses')
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  ShowAllDiscourses
);
