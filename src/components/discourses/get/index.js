// @flow

import khange, { kheck } from 'khange';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getParam } from '@client/selectors/router';
import { flowRight } from 'lodash';
import * as discourseSelectors from '@client/selectors/discourses';
import Discourse from '@client/models/Discourse'
import { get } from '@client/actions/discourses';
import { bindActionCreators } from '@client/utils/components';
import { formParent } from '@client/hocs';

const getDiscourseId = getParam('discourseId');

export type $stateProps = {
  id: $$id,
  discourse: Discourse,
};

type $dispatchProps = {
  get: Function,
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  id: getDiscourseId,
  discourse: discourseSelectors.find(getDiscourseId)
});

export const mapDispatchToProps = (dispatch: $$dispatch): $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      get
    },
    dispatch
  );

  export const onKhange = (props: $$formParentProps & $stateProps & $dispatchProps) => {
  props.get(props.id).then(() => {
    if (props.reinitializeForm) props.reinitializeForm.go();
  });
};

export default flowRight([
  formParent,
  connect(mapStateToProps, mapDispatchToProps),
  khange(kheck('id'), onKhange)
])