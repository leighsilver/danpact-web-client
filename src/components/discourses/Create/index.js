// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';

import { Button } from 'ui-kit';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { validationSchema } from '@client/utils/discourses';
import { create as createDiscourse, goToDiscourse } from '@client/actions/discourses';
import uuid from 'uuid/v1';
import DiscourseForm from '../Form';

type $ownProps = {
  
}

type $stateProps = {

}

type $dispatchProps = {
  createDiscourse: Function,
  goToDiscourse: Function,
}

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class CreateDiscourse extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <DiscourseForm fields={this.props.fields} />
        <Button
          primary
          disabled={!props.isValid}
          onClick={this.props.handleSubmit}
        >
          Send Interview Request
        </Button>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      createDiscourse,
      goToDiscourse
    },
    dispatch
  );

export default flowRight([
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: () => ({
      content: '',
    }),
    validationSchema,
    handleSubmit: (values, { props }) => {
      const id = uuid();
      props.goToDiscourse(id);
      return props.createDiscourse({ ...values, id });
    }
  })
])(CreateDiscourse);
