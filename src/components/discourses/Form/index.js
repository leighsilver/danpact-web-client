// @flow
import * as React from 'react';
import { flowRight } from 'lodash';
import { TextInput } from 'ui-kit';

type $props = {
  fields: Object;
};

export class DiscourseForm extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <TextInput rows={5} multiline {...props.fields.content} />
      </React.Fragment>
    );
  }
}

export default flowRight()(DiscourseForm);
