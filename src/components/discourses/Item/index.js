// @flow
import * as React from 'react';
import { ULItem } from 'ui-kit';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import * as discourseSelectors from '@client/selectors/discourses';
import { goToEditDiscourse, goToDiscourse } from '@client/actions/discourses';
import { bindActionCreators } from '@client/utils/components';
import Discourse from '@client/models/Discourse'

type $stateProps = {
  discourse: Discourse
};

type $ownProps = {
  id: $$id,
  canEdit?: boolean,
};

type $dispatchProps = {
  goToEditDiscourse: Function,
  goToDiscourse: Function,
};

type $props = $ownProps & $stateProps & $dispatchProps;

export class DiscourseItem extends React.PureComponent<$props> {
  render() {
    const { canEdit, discourse, goToEditDiscourse, goToDiscourse, ...props } = this.props;
    return (
      <ULItem
        {...props}
        onClick={canEdit ? goToEditDiscourse : goToDiscourse}
        caption={discourse.name}
      />
    );
  }
}

export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  discourse: discourseSelectors.find()
});

export const mapDispatchToProps = (dispatch: $$dispatch, props: $props) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToEditDiscourse: () => goToEditDiscourse(props.id),
      goToDiscourse: () => goToDiscourse(props.id)
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(DiscourseItem);
