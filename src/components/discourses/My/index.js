// @flow
import 'react-table/react-table.css'
import React from 'react';
import { UL, ULItem } from 'ui-kit';
import { flowRight } from 'lodash';
import { push } from '@client/actions/router';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { getDiscourses } from '@client/actions/users';
import { bindActionCreators } from '@client/utils/components';
import { createStructuredSelector, createSelector } from 'reselect';
import { getRelated } from '@client/selectors/users';
import { get, findRelationship } from '@client/selectors/discourses';
import { findEntity } from '@client/selectors/companies';
import ReactTable from 'react-table'
import { displayStatus } from '@client/models/Discourse';
import DiscourseItem from '../Item';

type $stateProps = {
  discourses: Array<Object>
};
type $dispatchProps = {
  goToDiscourse: Function,
  getDiscourses: Function,
};
type $ownProps = {
  canEdit: boolean,
  userId: $$id,
};

type $props = $stateProps & $dispatchProps & $ownProps;

export class MyDiscourses extends React.PureComponent<$props> {
  componentWillMount() {
    this.props.getDiscourses();
  }
  render() {
    const columns = [{
      Header: 'Company',
      accessor: 'company.name',
      Cell: props => {
        return <span onClick={this.props.goToDiscourse.bind(this, props.original.id)}>{props.value}</span>
      }
    },
    {
      Header: 'Compensation',
      accessor: 'compensation' // String-based value accessors!
    },
    {
      Header: 'Vacation',
      accessor: 'vacation' // String-based value accessors!
    },
    {
      Header: 'Status',
      accessor: 'status',
      Cell: props => {
        return <span className='number'>{props.value}</span>
      }
    }]
    const { canEdit } = this.props;
    return (
      <ReactTable
        data={this.props.discourses}
        columns={columns}
      />
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, props: $ownProps) : $Exact<$dispatchProps> =>
  bindActionCreators(
    {
      goToDiscourse: (id) => push(`/discourses/${id}`),
      getDiscourses: ()=>getDiscourses(props.userId || 'fakeid')
    },
    dispatch
  );


const getDiscourseIds = getRelated('discourses', (state, props)=>props.userId || 'fakeid')
export const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  discourses: createSelector([
    get(getDiscourseIds),
    findRelationship('company'),
    findEntity(),
  ], (discourses, companyRelationship, companyData)=>{
    return discourses.toJS().map((discourse)=>{
      return {
        ...discourse,
        company: companyData.get(companyRelationship.get(discourse.id)),
        status: displayStatus(discourse.status),
      }
    })
  })
});
export default flowRight([connect(mapStateToProps, mapDispatchToProps)])(
  MyDiscourses
);
