// @flow
import React from 'react';
import { flowRight } from 'lodash';
import UL from 'ui-kit/UL';
import CommentItem from 'components/comments/Item';
import { connect } from 'react-redux';
import { getRelated, findRelated } from '@client/selectors/discourses';
import getDiscourse from '../get';
import { createStructuredSelector } from 'reselect';
import { List } from 'immutable';
import PositionBreakdown from 'components/positions/Breakdown';
import CompanyBreakdown from 'components/companies/Breakdown';
import TalentStatus from 'components/discourses/TalentStatus';
import CreateComment from 'components/comments/Create';

import type { $stateProps as $getStateProps } from '../get';

type $stateProps = {
  commentIds: List<$$id>,
  positionId: $$id,
  companyId: $$id,
}
type $props = $getStateProps & $stateProps;

export class ShowDiscourse extends React.PureComponent<$props> {
  
  render() {
    const { props } = this;
    return (
      <div>
        <PositionBreakdown id={props.positionId} />
        <CompanyBreakdown id={props.companyId} />
        <TalentStatus status={props.discourse.status} />
        <UL>
          {
            props.commentIds.map((id)=>{
              return <CommentItem id={id} key={id} />
            })
          }
          <CreateComment discourseId={props.id} />
        </UL>
      </div>
    );
  }
}

const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  commentIds: getRelated('comments'),
  positionId: findRelated('position'),
  companyId: findRelated('company'),
});

export default flowRight([getDiscourse, connect(mapStateToProps)])(ShowDiscourse);

