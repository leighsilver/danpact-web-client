// @flow
import * as React from 'react';
import { Route } from 'react-router';
import ShowAll from './ShowAll';
import Create from './Create';
import Show from './Show';
import Edit from './Edit';
import My from './My';

export default [
  <Route exact key="ShowAll" path="/discourses" component={ShowAll} />,
  <Route exact key="Create" path="/discourses/create" component={Create} />,
  <Route exact key="My" path="/discourses/my" component={My} />,
  <Route exact key="Edit" path="/discourses/:discourseId/edit" component={Edit} />,
  <Route exact key="Show" path="/discourses/:discourseId" component={Show} />,
]
