// @flow
import * as React from 'react'
import { flowRight, bindActionCreators } from '@client/utils/components'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import Button from 'ui-kit/Button';
import { status } from '@client/models/Discourse';

type $stateProps = {}
type $ownProps = {}
type $dispatchProps = {}
type $props = $stateProps & $dispatchProps & $ownProps
export class  DiscourseTalentStatus extends React.PureComponent<$props> {
  getTextAndClick = ()=>{
    switch(this.props.status){
      case status.INTERVIEW_OFFERED:
        return {
          children: 'Accept Interview Request',
          onClick: this.props.acceptInterview, 
        }
      default:
        return {
          children: 'Offer Interview',
          onClick: this.props.offerInterview
        }
    }
  }
  render(){
    const { ...props } = this.props
    return  <Button {...this.getTextAndClick()} />
  }
}

const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
})

const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> => bindActionCreators({
}, dispatch)

export default flowRight([
  connect(mapStateToProps, mapDispatchToProps)
])( DiscourseTalentStatus)