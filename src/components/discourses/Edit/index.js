// @flow

import React from 'react';
import { flowRight } from 'lodash';
import { form } from '@client/hocs';
import { connect } from 'react-redux';
import { bindActionCreators } from '@client/utils/components';
import { update as updateDiscourse } from '@client/actions/discourses';
import { validationSchema } from '@client/utils/discourses';
import DiscourseForm from '../Form';
import getDiscourse from '../get';
import type { $stateProps as $getDiscourseProps} from '../get'

type $stateProps = {};
type $dispatchProps = {
  updateDiscourse: Function,
};
type $ownProps = $getDiscourseProps;

type $props = $stateProps & $dispatchProps & $ownProps & $$formProps;

export class EditDiscourse extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <h1>{props.discourse.name}</h1>
        <DiscourseForm fields={this.props.fields} />
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch, { id }: $ownProps) =>
  bindActionCreators(
    {
      updateDiscourse: values => updateDiscourse(id, values),
    },
    dispatch
  );

export default flowRight([
  getDiscourse,
  connect(null, mapDispatchToProps),
  form({
    mapPropsToValues: ({ discourse }) => ({
      name: discourse.name,
      purpose: discourse.purpose,
      notes: discourse.notes,
      instructives: discourse.instructives,
      scope: discourse.scope,
      refs: discourse.refs
    }),
    validationSchema,
    handleChange: props => {
      return (key, onChange) => {
        return value => {
          onChange(value);
          return props.updateDiscourse({ [key]: value });
        };
      };
    },
  }),
])(EditDiscourse);
