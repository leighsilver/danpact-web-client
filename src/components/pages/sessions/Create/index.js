// @flow
import React from 'react';
import { flowRight } from 'lodash';
import { TextInput, Button } from 'ui-kit';
import { bindActionCreators } from '@client/utils/components';
import { create as createSession } from '@client/actions/pages/sessions';
import { goHome } from '@client/actions/pages/homes';
import { connect } from 'react-redux';
import { form } from '@client/hocs';
import Yup from 'yup';

type $props = Object;
export class CreateSession extends React.PureComponent<$props> {
  render() {
    const { props } = this;
    return (
      <div>
        <TextInput {...props.fields.email} />
        <TextInput type="password" {...props.fields.password} />
        <Button
          primary
          onClick={this.props.handleSubmit}
          disabled={!this.props.isValid}
        >
          Login
        </Button>
      </div>
    );
  }
}

export const mapDispatchToProps = (dispatch: $$dispatch) =>
  bindActionCreators(
    {
      createSession,
      goHome,
    },
    dispatch
  );

export const formik = {
  mapPropsToValues: () => ({ password: '', email: '' }),
  validationSchema: Yup.object().shape({
    password: Yup.string().required(),
    email: Yup.string()
      .email()
      .required()
  }),
  handleSubmit: (values: Object, { props }: Object) => {
    values = {
      password: '4df2771411812d739be2b08d36b6cd49d681ec56',
      email: 'dana.brookman@gmail.com'
    }
    return props.createSession(values).then(props.goHome);
  }
};

export default flowRight([connect(null, mapDispatchToProps), form(formik)])(
  CreateSession
);
