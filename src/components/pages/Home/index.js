// @flow
import * as React from 'react'
import { flowRight, bindActionCreators } from '@client/utils/components'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import * as sessionSelectors from '@client/selectors/pages/sessions'
import EditTalent from 'components/talents/Edit';

type $stateProps = {}
type $ownProps = {}
type $dispatchProps = {}
type $props = $stateProps & $dispatchProps & $ownProps
export class Home extends React.PureComponent<$props> {
  render(){
    const { ...props } = this.props
    return <div>
      {
        props.session.type === 'talent' ? 
        <EditTalent /> : <div />
      }
    </div>
  }
}

const mapStateToProps : $$selectorExact<$stateProps> = createStructuredSelector({
  // session: sessionSelectors.find(),
  session: ()=>({type: 'talent'})
})

const mapDispatchToProps = (dispatch: $$dispatch) : $Exact<$dispatchProps> => bindActionCreators({
}, dispatch)

export default flowRight([
  connect(mapStateToProps, mapDispatchToProps)
])(Home)